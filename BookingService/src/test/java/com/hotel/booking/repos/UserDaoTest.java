package com.hotel.booking.repos;

import com.hotel.booking.model.Role;
import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserDao userDao;

    @Test
    public void saveAndLoad() {
        User user = new User();
        user.setLogin("testLogin");
        user.setPassword("testPassword");
        user.setEmail("testEmail@test.com");
        user.setPhone("+79999999999");
        user.setRole(Role.USER);
        user.setStatus(Status.ENABLED);
        user = userDao.saveAndFlush(user);

        User loadUser = userDao.findById(user.getId()).orElse(null);
        assertEquals(user, loadUser);
    }

    @Test
    public void findByLogin() {
        User user1 = new User();
        user1.setLogin("login1");
        user1.setPassword("password1");
        user1.setStatus(Status.ENABLED);
        user1.setRole(Role.ADMIN);
        entityManager.persistAndFlush(user1);

        User user2 = new User();
        user2.setLogin("login2");
        user2.setPassword("password2");
        user2.setStatus(Status.ENABLED);
        user2.setRole(Role.ADMIN);
        user2 = entityManager.persistAndFlush(user2);

        User user3 = new User();
        user3.setLogin("login3");
        user3.setPassword("password3");
        user3.setStatus(Status.ENABLED);
        user3.setRole(Role.ADMIN);
        entityManager.persistAndFlush(user3);

        User loadUser = userDao.findByLogin("login2");
        assertEquals(user2, loadUser);
    }

    @Test
    public void uniqueLogin() {
        User user1 = new User();
        user1.setLogin("login1");
        user1.setPassword("password1");
        user1.setStatus(Status.ENABLED);
        user1.setRole(Role.ADMIN);
        entityManager.persistAndFlush(user1);

        User user2 = new User();
        user2.setLogin("login1");
        user2.setPassword("password2");
        user2.setStatus(Status.ENABLED);
        user2.setRole(Role.USER);
        assertThrows(Exception.class, () -> userDao.save(user2));
    }

}
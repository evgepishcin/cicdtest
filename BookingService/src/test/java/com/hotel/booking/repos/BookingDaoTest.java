package com.hotel.booking.repos;

import com.hotel.booking.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BookingDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BookingDao bookingDao;

    @Test
    public void saveAndLoad() {
        User user = new User();
        user.setLogin("testLogin");
        user.setPassword("testPassword");
        user.setEmail("testEmail@test.com");
        user.setPhone("+79999999999");
        user.setRole(Role.USER);
        user.setStatus(Status.ENABLED);
        user = entityManager.persistAndFlush(user);

        Booking booking = new Booking();
        booking.setUser(user);
        booking.setStartDate(LocalDateTime.of(2030, 1, 10, 0, 0));
        booking.setEndDate(LocalDateTime.of(2030, 1, 20, 0, 0));
        booking.setBookingStatus(BookingStatus.OPENED);
        booking.setHighPrice(100);
        booking.setRequiredRating(5);
        booking.setRoomCapacity(2);
        booking = bookingDao.saveAndFlush(booking);

        Booking loadBooking = bookingDao.findById(booking.getId()).orElse(null);
        assertEquals(booking, loadBooking);
    }
}
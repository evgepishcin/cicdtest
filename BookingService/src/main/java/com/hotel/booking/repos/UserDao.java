package com.hotel.booking.repos;

import com.hotel.booking.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {

    User findByLogin(String login);
}

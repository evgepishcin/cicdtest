package com.hotel.booking.repos;

import com.hotel.booking.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookingDao extends JpaRepository<Booking, Integer> {

    //@Query("select booking from Booking booking where booking.user.id = ?1")
    List<Booking> findByUserId(int userId);

}

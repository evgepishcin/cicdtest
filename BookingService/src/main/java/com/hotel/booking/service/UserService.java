package com.hotel.booking.service;

import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import com.hotel.booking.repos.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserService {

    private static final Logger log = LogManager.getLogger();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    public User createUser(User user) {
        user.setId(0);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userDao.save(user);
    }

    public User setStatus(int userId, Status status) {
        User user = userDao.findById(userId).orElse(null);
        assert user != null;
        user.setStatus(status);
        return userDao.save(user);
    }

    public User changePhone(int userId, String phone) {
        User user = userDao.findById(userId).orElse(null);
        assert user != null;
        user.setPhone(phone);
        return userDao.save(user);
    }

    public User changeEmail(int userId, String email) {
        User user = userDao.findById(userId).orElse(null);
        assert user != null;
        user.setEmail(email);
        return userDao.save(user);
    }


    public void deleteUser(int userId) {
        userDao.deleteById(userId);
    }

    public User getUser(int userId) {
        return userDao.findById(userId).orElse(null);
    }

    public User getByName(String name) {
        return userDao.findByLogin(name);
    }

    public List<User> getAllUsers() {
        return userDao.findAll();
    }
}

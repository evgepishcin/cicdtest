package com.hotel.booking.service;


import com.hotel.booking.model.Role;
import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import com.hotel.booking.repos.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class DefaultAdminCreatorService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @Value("${create-default-admin}")
    private boolean createNeeded = false;

    @Value("${admin-name}")
    private String login;

    @Value("${admin-password}")
    private String password;

    @PostConstruct
    public void createAdmin() {
        if (!createNeeded) return;
        User user = userDao.findByLogin(login);
        if (user != null) return;
        user = new User();
        user.setId(0);
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(Role.ADMIN);
        user.setStatus(Status.ENABLED);
        userService.createUser(user);
    }
}

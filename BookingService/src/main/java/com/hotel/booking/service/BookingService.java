package com.hotel.booking.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.hotel.booking.dto.BookingDto;
import com.hotel.booking.dto.NewBookingDto;
import com.hotel.booking.model.Booking;
import com.hotel.booking.model.BookingStatus;
import com.hotel.booking.repos.BookingDao;
import lombok.SneakyThrows;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringWriter;
import java.util.List;

@Service
public class BookingService {

    private final BookingDao bookingDao;
    private final AmqpTemplate amqpTemplate;
    private final String fanoutSendingBooking;
    private final ObjectMapper objectMapper;

    @Autowired
    public BookingService(BookingDao bookingDao, AmqpTemplate amqpTemplate,
                          @Value("${fanout-sending-booking-name}") String fanoutSendingBooking,
                          ObjectMapper objectMapper) {
        this.bookingDao = bookingDao;
        this.amqpTemplate = amqpTemplate;
        this.fanoutSendingBooking = fanoutSendingBooking;
        this.objectMapper = objectMapper;
    }

    @SneakyThrows
    @Transactional
    public Booking addBooking(Booking booking) {
        booking.setBookingStatus(BookingStatus.OPENED);
        booking.setId(null);
        booking = bookingDao.save(booking);
        NewBookingDto dto = new NewBookingDto(booking);
        StringWriter stringWriter = new StringWriter();
        objectMapper.writeValue(stringWriter,dto);
        amqpTemplate.convertAndSend(fanoutSendingBooking, "", stringWriter.toString());
        return booking;
    }

    @Transactional
    public void readyBooking(Booking booking) {
        Booking restoredBooking = bookingDao.findById(booking.getId()).orElse(null);
        if (restoredBooking == null){
            return;
        }
        booking.setUser(restoredBooking.getUser());
        bookingDao.save(booking);
    }

    public List<Booking> getByUserId(int userId) {
        return bookingDao.findByUserId(userId);
    }
}

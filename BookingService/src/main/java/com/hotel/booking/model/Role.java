package com.hotel.booking.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role {

    USER(Set.of(Permission.BOOKING, Permission.CHANGE_PHONE, Permission.CHANGE_EMAIL, Permission.CHANGE_SELF_INFO)),
    ADMIN(Set.of(Permission.CRUD_USERS, Permission.BOOKING,
            Permission.CHANGE_EMAIL, Permission.CHANGE_PHONE, Permission.CHANGE_SELF_INFO));

    private final Set<String> permissions;

    Role(Set<String> permissions) {
        this.permissions = permissions;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}

package com.hotel.booking.model;

public interface Permission {
    String BOOKING = "BOOKING";
    String CHANGE_EMAIL = "CHANGE_EMAIL";
    String CHANGE_PHONE = "CHANGE_PHONE";
    String CRUD_USERS = "CRUD_USERS";
    String CHANGE_SELF_INFO = "CHANGE_SELF_INFO";
}

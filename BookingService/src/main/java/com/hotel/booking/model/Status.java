package com.hotel.booking.model;

public enum Status {
    ENABLED, DISABLED;
}

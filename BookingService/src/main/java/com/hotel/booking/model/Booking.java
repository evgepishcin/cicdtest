package com.hotel.booking.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "booking_status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    BookingStatus bookingStatus;

    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    @Column(name = "end_date", nullable = false)
    LocalDateTime endDate;

    @Column(name = "room_capacity")
    private Integer roomCapacity;

    @Column(name = "high_price")
    private Integer highPrice;

    @Column(name = "required_rating")
    private Integer requiredRating;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return Objects.equals(id, booking.id) &&
                bookingStatus == booking.bookingStatus &&
                Objects.equals(startDate, booking.startDate) &&
                Objects.equals(endDate, booking.endDate) &&
                Objects.equals(roomCapacity, booking.roomCapacity) &&
                Objects.equals(highPrice, booking.highPrice) &&
                Objects.equals(requiredRating, booking.requiredRating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

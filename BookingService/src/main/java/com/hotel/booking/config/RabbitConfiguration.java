package com.hotel.booking.config;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {

    @Value("${fanout-sending-booking-name}")
    private String fanoutSendingBooking;

    @Value("${fanout-ready-booking-name}")
    private String fanoutReadyBooking;

    @Value("${queue-sending-booking-name}")
    private String queueSendingBooking;

    @Value("${queue-ready-booking-name}")
    private String queueReadyBooking;

    @Bean
    public Declarables fanoutBindings() {
        Queue fanoutQueue1 = new Queue(queueSendingBooking, false);
        Queue fanoutQueue2 = new Queue(queueReadyBooking, false);
        FanoutExchange fanoutExchange1 = new FanoutExchange(fanoutSendingBooking);
        FanoutExchange fanoutExchange2 = new FanoutExchange(fanoutReadyBooking);
        return new Declarables(
                fanoutQueue1,
                fanoutQueue2,
                fanoutExchange1,
                fanoutExchange2,
                BindingBuilder.bind(fanoutQueue1).to(fanoutExchange1),
                BindingBuilder.bind(fanoutQueue2).to(fanoutExchange2)
        );
    }

//    @Bean
//    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
//        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
//        return rabbitTemplate;
//    }
//
//    @Bean
//    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
//        return new Jackson2JsonMessageConverter();
//    }
}

package com.hotel.booking.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hotel.booking.dto.BookingDto;
import com.hotel.booking.model.Booking;
import com.hotel.booking.service.BookingService;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AMQPListener {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    BookingService bookingService;

    @SneakyThrows
    @RabbitListener(queues = {"${queue-ready-booking-name}"})
    public void onMessage(String message){
        BookingDto bookingDto = objectMapper.readValue(message, BookingDto.class);
        Booking booking = bookingDto.toBooking();
        bookingService.readyBooking(booking);
    }
}

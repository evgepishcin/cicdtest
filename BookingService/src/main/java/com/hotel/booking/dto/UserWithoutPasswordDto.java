package com.hotel.booking.dto;

import com.hotel.booking.model.Role;
import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import lombok.Data;

@Data
public class UserWithoutPasswordDto {

    private Integer id;
    private String login;
    private Role role;
    private Status status;
    private String phone;
    private String email;

    public UserWithoutPasswordDto() {
    }

    public UserWithoutPasswordDto(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.role = user.getRole();
        this.status = user.getStatus();
        this.phone = user.getPhone();
        this.email = user.getEmail();
    }

}

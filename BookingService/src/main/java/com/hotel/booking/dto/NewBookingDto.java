package com.hotel.booking.dto;

import com.hotel.booking.model.Booking;
import com.hotel.booking.model.BookingStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class NewBookingDto {

    private Integer id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer roomCapacity;
    private Integer highPrice;
    private Integer requiredRating;

    public NewBookingDto(Booking booking) {
        this.id = booking.getId();
        this.startDate = booking.getStartDate();
        this.endDate = booking.getEndDate();
        this.roomCapacity = booking.getRoomCapacity();
        this.highPrice = booking.getHighPrice();
        this.requiredRating = booking.getRequiredRating();
    }

    public Booking toBooking() {
        Booking booking = new Booking();
        booking.setId(id);
        booking.setBookingStatus(BookingStatus.OPENED);
        booking.setStartDate(startDate);
        booking.setEndDate(endDate);
        booking.setRoomCapacity(roomCapacity);
        booking.setHighPrice(highPrice);
        booking.setRequiredRating(requiredRating);
        return booking;
    }
}

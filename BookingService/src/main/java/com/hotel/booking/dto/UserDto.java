package com.hotel.booking.dto;

import com.hotel.booking.model.Role;
import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {

    private Integer id;
    private String login;
    private String password;
    private Role role;
    private Status status;
    private String phone;
    private String email;

    public UserDto(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.status = user.getStatus();
        this.phone = user.getPhone();
        this.email = user.getEmail();
    }

    public User toUser() {
        User user = new User();
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setStatus(status);
        user.setRole(role);
        user.setPhone(phone);
        user.setEmail(email);
        return user;
    }
}

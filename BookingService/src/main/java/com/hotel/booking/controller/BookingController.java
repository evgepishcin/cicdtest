package com.hotel.booking.controller;

import com.hotel.booking.dto.BookingDto;
import com.hotel.booking.dto.NewBookingDto;
import com.hotel.booking.model.Booking;
import com.hotel.booking.model.Permission;
import com.hotel.booking.model.User;
import com.hotel.booking.service.BookingService;
import com.hotel.booking.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    BookingService bookingService;

    @Autowired
    UserService userService;

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('" + Permission.BOOKING + "')")
    public ResponseEntity<BookingDto> addBooking(@RequestBody NewBookingDto newBookingDto, Principal principal) {
        User user = userService.getByName(principal.getName());
        Booking booking = newBookingDto.toBooking();
        booking.setUser(user);
        booking = bookingService.addBooking(booking);
        return new ResponseEntity<>(new BookingDto(booking), HttpStatus.OK);
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/getAll")
    @PreAuthorize("hasAuthority('" + Permission.BOOKING + "')")
    public ResponseEntity<List<BookingDto>> getAll(Principal principal) {
        User user = userService.getByName(principal.getName());
        List<BookingDto> bookingDtos = bookingService.getByUserId(user.getId())
                .stream()
                .map(BookingDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(bookingDtos, HttpStatus.OK);
    }
}

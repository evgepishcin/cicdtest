package com.hotel.booking.controller;

import com.hotel.booking.dto.AuthenticationRequestDto;
import com.hotel.booking.dto.UserDto;
import com.hotel.booking.dto.UserWithoutPasswordDto;
import com.hotel.booking.model.Permission;
import com.hotel.booking.model.Role;
import com.hotel.booking.model.Status;
import com.hotel.booking.model.User;
import com.hotel.booking.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/create")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<UserWithoutPasswordDto> createUser(@RequestBody UserDto dto) {
        User user = dto.toUser();
        user = userService.createUser(user);
        UserWithoutPasswordDto dtoUser = new UserWithoutPasswordDto(user);
        return new ResponseEntity<>(dtoUser, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<UserWithoutPasswordDto> register(@RequestBody AuthenticationRequestDto dto) {
        User user = new User();
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());
        user.setRole(Role.USER);
        user.setStatus(Status.ENABLED);
        user = userService.createUser(user);
        UserWithoutPasswordDto dtoUser = new UserWithoutPasswordDto(user);
        return new ResponseEntity<>(dtoUser, HttpStatus.OK);
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/{id:\\d+}/delete")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<?> deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/{id:\\d+}/changeStatus")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<UserWithoutPasswordDto> changeStatus(@PathVariable int id, @RequestParam Status status) {
        User user = userService.setStatus(id, status);
        UserWithoutPasswordDto dtoUser = new UserWithoutPasswordDto(user);
        return new ResponseEntity<>(dtoUser, HttpStatus.OK);
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/changePhone")
    @PreAuthorize("hasAuthority('" + Permission.CHANGE_SELF_INFO + "')")
    public ResponseEntity<UserWithoutPasswordDto> changePhone(@RequestParam String phone, Principal principal) {
        User user = userService.getByName(principal.getName());
        user = userService.changePhone(user.getId(), phone);
        return new ResponseEntity<>(new UserWithoutPasswordDto(user), HttpStatus.OK);

    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PostMapping("/changeEmail")
    @PreAuthorize("hasAuthority('" + Permission.CHANGE_SELF_INFO + "')")
    public ResponseEntity<UserWithoutPasswordDto> changeEmail(@RequestParam String email, Principal principal) {
        User user = userService.getByName(principal.getName());
        user = userService.changeEmail(user.getId(), email);
        return new ResponseEntity<>(new UserWithoutPasswordDto(user), HttpStatus.OK);
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @GetMapping("/{id:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<UserWithoutPasswordDto> getUser(@PathVariable int id) {
        User user = userService.getUser(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            UserWithoutPasswordDto userWp = new UserWithoutPasswordDto(user);
            return new ResponseEntity<>(userWp, HttpStatus.OK);
        }
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<List<UserWithoutPasswordDto>> getAllUsers() {
        List<UserWithoutPasswordDto> users = userService.getAllUsers()
                .stream()
                .map(UserWithoutPasswordDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}

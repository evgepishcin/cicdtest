-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: hoteladministrator
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (3,'Андрей','Кузнецов',NULL,NULL,NULL,NULL),(4,'Илья','Олегович',NULL,NULL,NULL,NULL),(5,'Никита','Вячеславович',NULL,NULL,NULL,'nnnikitaa@mail.ru'),(6,'Анна','Сергеевна',NULL,NULL,NULL,NULL),(7,'Иван','Николаевич',NULL,NULL,NULL,NULL),(8,'Дмитрий','Разумов','Дмитриевич','1998-09-15','+72946327744','dmdmdm@mail.ru'),(9,'Александр','Пирожков','','2001-02-20','+78469145867','');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (1,2,'SERVED',2,1000),(2,3,'SERVED',2,1100),(3,2,'REPAIRED',2,2000),(4,1,'SERVED',4,700);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roomhistory`
--

LOCK TABLES `roomhistory` WRITE;
/*!40000 ALTER TABLE `roomhistory` DISABLE KEYS */;
INSERT INTO `roomhistory` VALUES (1,1,3,'2020-01-05 00:00:00','2020-01-15 00:00:00'),(2,1,4,'2020-08-10 00:00:00','2020-10-10 00:00:00'),(3,1,5,'2020-01-16 00:00:00','2020-01-28 00:00:00'),(4,1,6,'2020-08-10 00:00:00','2020-10-10 00:00:00'),(5,1,3,'2019-01-01 00:00:00','2019-01-03 00:00:00'),(6,1,4,'2019-02-01 00:00:00','2019-02-03 00:00:00'),(7,1,4,'2018-02-01 00:00:00','2018-02-03 00:00:00'),(14,4,7,'2020-01-01 00:00:00','2020-01-05 00:00:00'),(16,2,9,'2020-08-17 05:12:21','2020-08-17 05:15:16');
/*!40000 ALTER TABLE `roomhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'массаж','массаж спины',300),(2,'tv+','более 100 канало на тв в номере',500),(3,'Еда +','Еще бооольше еды',1800);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `servicehistory`
--

LOCK TABLES `servicehistory` WRITE;
/*!40000 ALTER TABLE `servicehistory` DISABLE KEYS */;
INSERT INTO `servicehistory` VALUES (1,2,4,'2020-08-11 00:00:00'),(2,3,6,'2020-08-23 00:00:00');
/*!40000 ALTER TABLE `servicehistory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-17  5:47:43

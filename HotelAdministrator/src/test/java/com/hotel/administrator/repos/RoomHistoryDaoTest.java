package com.hotel.administrator.repos;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.Person;
import com.hotel.administrator.model.RoomHistory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomHistoryDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RoomHistoryDao roomHistoryDao;

    @Test
    public void getAll() {
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            entityManager.persistAndFlush(person);
            entityManager.persistAndFlush(room);
            if (i > 5) {
                RoomHistory history = new RoomHistory();
                history.setPerson(person);
                history.setHotelRoom(room);
                history.setStartDate(LocalDateTime.MIN);
                history.setEndDate(LocalDateTime.MAX);
                entityManager.persistAndFlush(history);
            }
        }
        List<RoomHistory> histories = roomHistoryDao.findAll();
        assertEquals(4, histories.size());
    }

    @Test
    public void deleteOldRecords() {
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Person person = new Person();
            people.add(entityManager.persistAndFlush(person));
        }
        HotelRoom room = new HotelRoom();
        room.setRoomNumber(312221);
        room.setMaxPersonCount(3);
        room.setStatus(HotelRoom.RoomStatus.SERVED);
        room.setPrice(1000);
        room.setRating(5);
        room = entityManager.persistAndFlush(room);
        for (int i = 0; i < 5; i++) {
            RoomHistory history = new RoomHistory();
            Person person = people.get(i);
            history.setPerson(person);
            history.setHotelRoom(room);
            history.setStartDate(LocalDateTime.of(2000 + i, 1, 1, 0, 0));
            history.setEndDate(LocalDateTime.of(2000 + i, 1, 2, 0, 0));
            entityManager.persistAndFlush(history);
        }
        roomHistoryDao.deleteOldRecords(room.getRoomNumber(), 1, LocalDateTime.of(2002, 11, 11, 0, 0));
        List<RoomHistory> afterClean = roomHistoryDao.findAll();
        assertEquals(2, afterClean.size());
    }

    @Test
    public void getRoomHistoryBySettledPerson() {
        List<HotelRoom> rooms = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(5);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000);
            room.setRating(5);
            rooms.add(entityManager.persistAndFlush(room));
        }
        Person person1 = new Person();
        person1 = entityManager.persistAndFlush(person1);
        RoomHistory history1 = new RoomHistory();
        history1.setPerson(person1);
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 3, 10, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 11, 10, 0, 0));
        entityManager.persistAndFlush(history1);
        RoomHistory history = roomHistoryDao.getRoomHistoryBySettledPerson(person1.getId(),
                LocalDateTime.of(2020, 11, 11, 0, 0));
        assertNull(history);

        Person person2 = new Person();
        Person person3 = new Person();
        person2 = entityManager.persistAndFlush(person2);
        person3 = entityManager.persistAndFlush(person3);
        RoomHistory history2 = new RoomHistory();
        history2.setPerson(person2);
        history2.setHotelRoom(rooms.get(1));
        history2.setStartDate(LocalDateTime.of(2020, 2, 10, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 3, 10, 0, 0));
        history2 = entityManager.persistAndFlush(history2);
        RoomHistory history3 = new RoomHistory();
        history3.setPerson(person3);
        history3.setHotelRoom(rooms.get(1));
        history3.setStartDate(LocalDateTime.of(2020, 2, 11, 0, 0));
        history3.setEndDate(LocalDateTime.of(2020, 3, 12, 0, 0));
        entityManager.persistAndFlush(history3);
        RoomHistory historyPerson2 = roomHistoryDao.getRoomHistoryBySettledPerson(person2.getId(),
                LocalDateTime.of(2020, 2, 28, 0, 0));
        assertEquals(history2, historyPerson2);
    }

    @Test
    public void getWillBeFree() {
        List<HotelRoom> rooms = new ArrayList<>();
        List<Person> people = new ArrayList<>();
        List<RoomHistory> willFree = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(5);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000);
            room.setRating(5);
            rooms.add(entityManager.persistAndFlush(room));
            Person person = new Person();
            people.add(entityManager.persistAndFlush(person));
        }
        RoomHistory history1 = new RoomHistory();
        history1.setPerson(people.get(0));
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 1, 10, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 1, 20, 0, 0));
        willFree.add(entityManager.persistAndFlush(history1));

        RoomHistory history2 = new RoomHistory();
        history2.setPerson(people.get(1));
        history2.setHotelRoom(rooms.get(1));
        history2.setStartDate(LocalDateTime.of(2020, 10, 10, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 11, 20, 0, 0));
        entityManager.persistAndFlush(history2);

        RoomHistory history3 = new RoomHistory();
        history3.setPerson(people.get(2));
        history3.setHotelRoom(rooms.get(2));
        history3.setStartDate(LocalDateTime.of(2020, 2, 10, 0, 0));
        history3.setEndDate(LocalDateTime.of(2020, 2, 20, 0, 0));
        willFree.add(entityManager.persistAndFlush(history3));

        RoomHistory history4 = new RoomHistory();
        history4.setPerson(people.get(3));
        history4.setHotelRoom(rooms.get(3));
        history4.setStartDate(LocalDateTime.of(2020, 5, 10, 0, 0));
        history4.setEndDate(LocalDateTime.of(2020, 5, 20, 0, 0));
        willFree.add(entityManager.persistAndFlush(history4));

        RoomHistory history5 = new RoomHistory();
        history5.setPerson(people.get(4));
        history5.setHotelRoom(rooms.get(4));
        history5.setStartDate(LocalDateTime.of(2020, 10, 10, 0, 0));
        history5.setEndDate(LocalDateTime.of(2020, 10, 20, 0, 0));
        entityManager.persistAndFlush(history5);

        List<RoomHistory> needWillFree = roomHistoryDao.getWillBeFree(LocalDateTime.of(2020, 6, 1, 0, 0));
        assertTrue(willFree.size() == needWillFree.size() &&
                willFree.containsAll(needWillFree) && needWillFree.containsAll(willFree));
    }

    @Test
    public void getLastRoomHistoryByPersonId() {
        List<HotelRoom> rooms = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(5);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000);
            room.setRating(5);
            rooms.add(entityManager.persistAndFlush(room));
        }

        Person person = entityManager.persistAndFlush(new Person());

        RoomHistory history1 = new RoomHistory();
        history1.setPerson(person);
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 1, 10, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 1, 20, 0, 0));
        entityManager.persistAndFlush(history1);

        RoomHistory history2 = new RoomHistory();
        history2.setPerson(person);
        history2.setHotelRoom(rooms.get(0));
        history2.setStartDate(LocalDateTime.of(2020, 2, 10, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 2, 20, 0, 0));
        entityManager.persistAndFlush(history2);

        RoomHistory history3 = new RoomHistory();
        history3.setPerson(person);
        history3.setHotelRoom(rooms.get(0));
        history3.setStartDate(LocalDateTime.of(2020, 3, 10, 0, 0));
        history3.setEndDate(LocalDateTime.of(2020, 3, 20, 0, 0));
        history3 = entityManager.persistAndFlush(history3);

        Person person2 = entityManager.persistAndFlush(new Person());
        RoomHistory history4 = new RoomHistory();
        history4.setPerson(person2);
        history4.setHotelRoom(rooms.get(0));
        history4.setStartDate(LocalDateTime.of(2020, 4, 10, 0, 0));
        history4.setEndDate(LocalDateTime.of(2020, 4, 20, 0, 0));
        entityManager.persistAndFlush(history4);

        RoomHistory last = roomHistoryDao.getLastRoomHistoryByPersonId(person.getId());
        assertEquals(history3, last);
    }
}

package com.hotel.administrator.repos;

import com.hotel.administrator.model.Person;
import com.hotel.administrator.model.Service;
import com.hotel.administrator.model.ServiceHistory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ServiceHistoryDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ServiceHistoryDao serviceHistoryDao;

    @Test
    public void getServiceHistoryByPersonId() {
        Person checkPerson = null;
        List<ServiceHistory> expected = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Person person = entityManager.persistAndFlush(new Person());
            if (i == 5) {
                checkPerson = person;
            }
            for (int j = 0; j < 3; j++) {
                Service service = new Service();
                service.setTitle("test" + j);
                service.setAbout("TEST" + j);
                service.setPrice(500 + i + j);
                service = entityManager.persistAndFlush(service);
                ServiceHistory history = new ServiceHistory();
                history.setPerson(person);
                history.setService(service);
                history.setDate(LocalDateTime.of(2020, 10, i + 1, 0, 0));
                entityManager.persistAndFlush(history);
                if (i == 5) {
                    expected.add(history);
                }
            }
        }
        List<ServiceHistory> histories = serviceHistoryDao.getServiceHistoryByPersonId(checkPerson.getId());
        assertTrue(expected.size() == histories.size() &&
                expected.containsAll(histories) && histories.containsAll(expected));
    }
}
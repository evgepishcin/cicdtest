package com.hotel.administrator.repos;

import com.hotel.administrator.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PersonDao personDao;

    @Test
    public void getSettledPersons() {
        List<Person> people = new ArrayList<>();
        List<HotelRoom> rooms = new ArrayList<>();
        List<Person> settledPersons = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            people.add(entityManager.persistAndFlush(new Person()));
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            rooms.add(entityManager.persistAndFlush(room));
        }
        RoomHistory history1 = new RoomHistory();
        history1.setPerson(people.get(0));
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 1, 1, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 1, 10, 0, 0));
        entityManager.persistAndFlush(history1);

        RoomHistory history2 = new RoomHistory();
        history2.setPerson(people.get(1));
        history2.setHotelRoom(rooms.get(0));
        history2.setStartDate(LocalDateTime.of(2020, 10, 1, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 10, 10, 0, 0));
        entityManager.persistAndFlush(history2);
        settledPersons.add(people.get(1));

        RoomHistory history3 = new RoomHistory();
        history3.setPerson(people.get(2));
        history3.setHotelRoom(rooms.get(1));
        history3.setStartDate(LocalDateTime.of(2020, 4, 1, 0, 0));
        history3.setEndDate(LocalDateTime.of(2020, 5, 10, 0, 0));
        entityManager.persistAndFlush(history3);

        RoomHistory history4 = new RoomHistory();
        history4.setPerson(people.get(3));
        history4.setHotelRoom(rooms.get(1));
        history4.setStartDate(LocalDateTime.of(2020, 4, 1, 0, 0));
        history4.setEndDate(LocalDateTime.of(2020, 6, 10, 0, 0));
        entityManager.persistAndFlush(history4);
        settledPersons.add(people.get(3));

        RoomHistory history5 = new RoomHistory();
        history5.setPerson(people.get(4));
        history5.setHotelRoom(rooms.get(2));
        history5.setStartDate(LocalDateTime.of(2020, 1, 1, 0, 0));
        history5.setEndDate(LocalDateTime.of(2020, 2, 10, 0, 0));
        entityManager.persistAndFlush(history5);

        RoomHistory history6 = new RoomHistory();
        history6.setPerson(people.get(4));
        history6.setHotelRoom(rooms.get(2));
        history6.setStartDate(LocalDateTime.of(2020, 5, 1, 0, 0));
        history6.setEndDate(LocalDateTime.of(2020, 6, 10, 0, 0));
        entityManager.persistAndFlush(history6);
        settledPersons.add(people.get(4));

        List<Person> needSettled = personDao.getSettledPersons(LocalDateTime.of(2020, 6, 1, 0, 0));
        assertTrue(settledPersons.size() == needSettled.size() &&
                settledPersons.containsAll(needSettled) && needSettled.containsAll(settledPersons));
    }

    @Test
    public void getHotelRoomPersons() {
        LocalDateTime currentTime = LocalDateTime.of(2020, 6, 1, 0, 0);
        List<Person> people = new ArrayList<>();
        List<HotelRoom> rooms = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            people.add(entityManager.persistAndFlush(new Person()));
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            rooms.add(entityManager.persistAndFlush(room));
        }
        RoomHistory history1 = new RoomHistory();
        history1.setPerson(people.get(0));
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 1, 1, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 1, 10, 0, 0));
        entityManager.persistAndFlush(history1);

        RoomHistory history2 = new RoomHistory();
        history2.setPerson(people.get(1));
        history2.setHotelRoom(rooms.get(0));
        history2.setStartDate(LocalDateTime.of(2020, 10, 1, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 10, 10, 0, 0));
        entityManager.persistAndFlush(history2);

        RoomHistory history3 = new RoomHistory();
        history3.setPerson(people.get(2));
        history3.setHotelRoom(rooms.get(1));
        history3.setStartDate(LocalDateTime.of(2020, 4, 1, 0, 0));
        history3.setEndDate(LocalDateTime.of(2020, 5, 10, 0, 0));
        entityManager.persistAndFlush(history3);

        RoomHistory history4 = new RoomHistory();
        history4.setPerson(people.get(3));
        history4.setHotelRoom(rooms.get(1));
        history4.setStartDate(LocalDateTime.of(2020, 4, 1, 0, 0));
        history4.setEndDate(LocalDateTime.of(2020, 6, 10, 0, 0));
        entityManager.persistAndFlush(history4);

        RoomHistory history5 = new RoomHistory();
        history5.setPerson(people.get(4));
        history5.setHotelRoom(rooms.get(2));
        history5.setStartDate(LocalDateTime.of(2020, 1, 1, 0, 0));
        history5.setEndDate(LocalDateTime.of(2020, 2, 10, 0, 0));
        entityManager.persistAndFlush(history5);

        RoomHistory history6 = new RoomHistory();
        history6.setPerson(people.get(4));
        history6.setHotelRoom(rooms.get(2));
        history6.setStartDate(LocalDateTime.of(2020, 5, 1, 0, 0));
        history6.setEndDate(LocalDateTime.of(2020, 6, 10, 0, 0));
        entityManager.persistAndFlush(history6);

        RoomHistory history7 = new RoomHistory();
        history7.setPerson(people.get(5));
        history7.setHotelRoom(rooms.get(2));
        history7.setStartDate(LocalDateTime.of(2020, 5, 1, 0, 0));
        history7.setEndDate(LocalDateTime.of(2020, 6, 11, 0, 0));
        entityManager.persistAndFlush(history7);

        List<Person> expected1 = List.of(people.get(1));
        List<Person> roomPersons1 = personDao.getHotelRoomPersons(rooms.get(0).getRoomNumber(), currentTime);
        assertTrue(expected1.size() == roomPersons1.size() &&
                expected1.containsAll(roomPersons1) && roomPersons1.containsAll(expected1));

        List<Person> expected2 = List.of(people.get(3));
        List<Person> roomPersons2 = personDao.getHotelRoomPersons(rooms.get(1).getRoomNumber(), currentTime);
        assertTrue(expected2.size() == roomPersons2.size() &&
                expected2.containsAll(roomPersons2) && roomPersons2.containsAll(expected2));

        List<Person> expected3 = List.of(people.get(4), people.get(5));
        List<Person> roomPersons3 = personDao.getHotelRoomPersons(rooms.get(2).getRoomNumber(), currentTime);
        assertTrue(expected3.size() == roomPersons3.size() &&
                expected3.containsAll(roomPersons3) && roomPersons3.containsAll(expected3));
    }

}
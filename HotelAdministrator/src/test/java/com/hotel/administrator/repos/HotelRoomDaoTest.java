package com.hotel.administrator.repos;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.Person;
import com.hotel.administrator.model.RoomHistory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HotelRoomDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private HotelRoomDao hotelRoomDao;

    @Test
    public void getAllHotelRoom() {
        List<HotelRoom> rooms = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            HotelRoom hotelRoom = new HotelRoom();
            hotelRoom.setRoomNumber(i + 1);
            hotelRoom.setMaxPersonCount(3);
            hotelRoom.setStatus(HotelRoom.RoomStatus.SERVED);
            hotelRoom.setPrice(1000 + i);
            hotelRoom.setRating(5);
            rooms.add(entityManager.persistAndFlush(hotelRoom));
        }

        List<HotelRoom> returnedRooms = hotelRoomDao.findAll();
        assertTrue(rooms.size() == returnedRooms.size() &&
                rooms.containsAll(returnedRooms) && returnedRooms.containsAll(rooms));
    }

    @Test
    public void checkAllEmpty() {
        List<HotelRoom> hotelRooms = null;
        try {
            hotelRooms = hotelRoomDao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert hotelRooms != null;
        assertEquals(0, hotelRooms.size());
    }

    @Test
    public void getHotelRoom() {
        HotelRoom checkRoom = null;
        for (int i = 0; i < 10; i++) {
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            if (i == 5) {
                checkRoom = entityManager.persistAndFlush(room);
            } else {
                entityManager.persistAndFlush(room);
            }
        }
        HotelRoom hotelRoom = hotelRoomDao.findById(checkRoom.getRoomNumber()).orElse(null);
        assertEquals(checkRoom, hotelRoom);
    }

    @Test
    public void getHotelRoomByPerson() {
        List<HotelRoom> rooms = new ArrayList<>();
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            people.add(entityManager.persistAndFlush(person));
            rooms.add(entityManager.persistAndFlush(room));
        }
        RoomHistory history1 = new RoomHistory();
        history1.setPerson(people.get(0));
        history1.setHotelRoom(rooms.get(0));
        history1.setStartDate(LocalDateTime.of(2020, 1, 1, 0, 0));
        history1.setEndDate(LocalDateTime.of(2020, 1, 3, 0, 0));
        entityManager.persistAndFlush(history1);

        RoomHistory history2 = new RoomHistory();
        history2.setPerson(people.get(1));
        history2.setHotelRoom(rooms.get(0));
        history2.setStartDate(LocalDateTime.of(2020, 2, 1, 0, 0));
        history2.setEndDate(LocalDateTime.of(2020, 2, 3, 0, 0));
        entityManager.persistAndFlush(history2);

        assertEquals(rooms.get(0), hotelRoomDao.getHotelRoomByPerson(people.get(1).getId(),
                LocalDateTime.of(2020, 1, 20, 0, 0)));
        assertNull(hotelRoomDao.getHotelRoomByPerson(people.get(0).getId(),
                LocalDateTime.of(2020, 1, 20, 0, 0)));
    }

    @Test
    public void getEmptyHotelRooms() {
        List<HotelRoom> emptyHotelRooms = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Person person = entityManager.persistAndFlush(new Person());
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            room = entityManager.persistAndFlush(room);
            LocalDateTime timeStart;
            LocalDateTime timeEnd;
            if (i % 2 == 0) {
                timeStart = LocalDateTime.of(2020, 1, i + 1, 0, 0);
                timeEnd = LocalDateTime.of(2020, 10, i + 2, 0, 0);
            } else {
                timeStart = LocalDateTime.of(2020, 1, i + 1, 0, 0);
                timeEnd = LocalDateTime.of(2020, 1, i + 2, 0, 0);
                if (i == 9){
                    timeStart = LocalDateTime.of(2020, 6, i + 1, 0, 0);
                    timeEnd = LocalDateTime.of(2020, 10, i + 2, 0, 0);
                }
                emptyHotelRooms.add(room);
            }
            RoomHistory history = new RoomHistory();
            history.setPerson(person);
            history.setHotelRoom(room);
            history.setStartDate(timeStart);
            history.setEndDate(timeEnd);
            entityManager.persistAndFlush(history);
        }

        List<HotelRoom> needEmpty = hotelRoomDao.getEmptyHotelRooms(LocalDateTime.of(2020, 5, 1, 0, 0));
        assertTrue(emptyHotelRooms.size() == needEmpty.size() &&
                emptyHotelRooms.containsAll(needEmpty) && needEmpty.containsAll(emptyHotelRooms));
    }

    @Test
    public void getEmptyHotelRoomInPeriod(){
        List<HotelRoom> rooms = new ArrayList<>();
        List<HotelRoom> emptyRooms = new ArrayList<>();
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            HotelRoom room = new HotelRoom();
            room.setRoomNumber(i + 1);
            room.setMaxPersonCount(3);
            room.setStatus(HotelRoom.RoomStatus.SERVED);
            room.setPrice(1000 + i);
            room.setRating(5);
            if (i == 4){
                room.setStatus(HotelRoom.RoomStatus.REPAIRED);
            }
            room = entityManager.persistAndFlush(room);
            rooms.add(room);
            people.add(entityManager.persistAndFlush(new Person()));
        }
        RoomHistory history1 = new RoomHistory();
        history1.setHotelRoom(rooms.get(0));
        history1.setPerson(people.get(0));
        history1.setStartDate(LocalDateTime.of(2020,1,1,0,0));
        history1.setEndDate(LocalDateTime.of(2020,2,1,0,0));
        emptyRooms.add(rooms.get(0));
        entityManager.persistAndFlush(history1);

        RoomHistory history2 = new RoomHistory();
        history2.setHotelRoom(rooms.get(1));
        history2.setPerson(people.get(1));
        history2.setStartDate(LocalDateTime.of(2020,8,1,0,0));
        history2.setEndDate(LocalDateTime.of(2020,9,1,0,0));
        emptyRooms.add(rooms.get(1));
        entityManager.persistAndFlush(history2);

        RoomHistory history3 = new RoomHistory();
        history3.setHotelRoom(rooms.get(2));
        history3.setPerson(people.get(2));
        history3.setStartDate(LocalDateTime.of(2020,1,1,0,0));
        history3.setEndDate(LocalDateTime.of(2020,9,1,0,0));
        entityManager.persistAndFlush(history3);

        emptyRooms.add(rooms.get(3));

        List<HotelRoom> needEmpty = hotelRoomDao.getEmptyHotelRoomInPeriod(LocalDateTime.of(2020,4,1,0,0),
                LocalDateTime.of(2020,6,1,0,0));
        assertTrue(emptyRooms.size() == needEmpty.size() &&
                emptyRooms.containsAll(needEmpty) && needEmpty.containsAll(emptyRooms));
    }

}
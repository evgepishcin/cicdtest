package com.hotel.administrator.service;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.Person;
import com.hotel.administrator.repos.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HotelServiceTest {

    @Autowired
    private HotelService hotelService;

    @MockBean
    private HotelRoomDao hotelRoomDao;

    @MockBean
    private PersonDao personDao;

    @MockBean
    private RoomHistoryDao roomHistoryDao;

    @MockBean
    private ServiceDao serviceDao;

    @MockBean
    private ServiceHistoryDao serviceHistoryDao;

    @Test
    public void settlePersonCorrectTest() {
        LocalDateTime dateTime = LocalDateTime.MAX;
        Person insertPerson = new Person();
        HotelRoom room = new HotelRoom();
        room.setMaxPersonCount(3);
        List<Person> people = new ArrayList<>();
        room.setStatus(HotelRoom.RoomStatus.SERVED);
        room.setRoomNumber(1);
        BDDMockito.given(this.personDao.findById(1)).willReturn(java.util.Optional.of(insertPerson));
        BDDMockito.given(this.hotelRoomDao.getHotelRoomByPerson(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(null);
        BDDMockito.given(this.hotelRoomDao.findById(1)).willReturn(java.util.Optional.of(room));
        BDDMockito.given(this.personDao.getHotelRoomPersons(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(people);
        Assertions.assertDoesNotThrow(
                () -> hotelService.settlePerson(1, 1, dateTime)
        );
    }

    @Test
    public void settlePersonDateEvictBeforeCurrentTest() {
        LocalDateTime dateTime = LocalDateTime.MIN;
        Person insertPerson = new Person();
        HotelRoom room = new HotelRoom();
        room.setMaxPersonCount(3);
        List<Person> people = new ArrayList<>();
        room.setStatus(HotelRoom.RoomStatus.SERVED);
        room.setRoomNumber(1);
        BDDMockito.given(this.personDao.findById(1)).willReturn(java.util.Optional.of(insertPerson));
        BDDMockito.given(this.hotelRoomDao.getHotelRoomByPerson(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(null);
        BDDMockito.given(this.hotelRoomDao.findById(1)).willReturn(java.util.Optional.of(room));
        BDDMockito.given(this.personDao.getHotelRoomPersons(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(people);
        Assertions.assertThrows(
                HotelServiceException.class,
                () -> hotelService.settlePerson(1, 1, dateTime)
        );
    }

    @Test
    public void settlePersonRoomRepair() {
        LocalDateTime dateTime = LocalDateTime.MAX;
        Person insertPerson = new Person();
        HotelRoom room = new HotelRoom();
        room.setMaxPersonCount(3);
        List<Person> people = new ArrayList<>();
        room.setStatus(HotelRoom.RoomStatus.REPAIRED);
        room.setRoomNumber(1);
        BDDMockito.given(this.personDao.findById(1)).willReturn(java.util.Optional.of(insertPerson));
        BDDMockito.given(this.hotelRoomDao.getHotelRoomByPerson(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(null);
        BDDMockito.given(this.hotelRoomDao.findById(1)).willReturn(java.util.Optional.of(room));
        BDDMockito.given(this.personDao.getHotelRoomPersons(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(people);
        Assertions.assertThrows(
                HotelServiceException.class,
                () -> hotelService.settlePerson(1, 1, dateTime)
        );
    }

    @Test
    public void settlePersonDontExistRoom() {
        LocalDateTime dateTime = LocalDateTime.MAX;
        Person insertPerson = new Person();
        List<Person> people = new ArrayList<>();
        BDDMockito.given(this.personDao.findById(1)).willReturn(java.util.Optional.of(insertPerson));
        BDDMockito.given(this.hotelRoomDao.getHotelRoomByPerson(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(null);
        BDDMockito.given(this.hotelRoomDao.findById(1)).willReturn(null);
        BDDMockito.given(this.personDao.getHotelRoomPersons(BDDMockito.eq(1),
                BDDMockito.any())).willReturn(people);
        Assertions.assertThrows(
                HotelServiceException.class,
                () -> hotelService.settlePerson(1, 1, dateTime)
        );
    }

}

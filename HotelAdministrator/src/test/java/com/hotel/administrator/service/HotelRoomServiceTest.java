package com.hotel.administrator.service;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.repos.HotelRoomDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HotelRoomServiceTest {

    @Autowired
    private HotelRoomService hotelRoomService;

    @MockBean
    private HotelRoomDao hotelRoomDao;

    @Test
    public void getRoomsByCriteria(){
        LocalDateTime start = LocalDateTime.of(2020,5,1,0,0);
        LocalDateTime end = LocalDateTime.of(2020,5,10,0,0);
        List<HotelRoom> rooms = new ArrayList<>();
        List<HotelRoom> filteredRooms = new ArrayList<>();

        HotelRoom room1 = new HotelRoom();
        room1.setRoomNumber(1);
        room1.setStatus(HotelRoom.RoomStatus.SERVED);
        room1.setMaxPersonCount(3);
        room1.setPrice(500);
        room1.setRating(5);
        rooms.add(room1);

        HotelRoom room2 = new HotelRoom();
        room2.setRoomNumber(2);
        room2.setStatus(HotelRoom.RoomStatus.SERVED);
        room2.setMaxPersonCount(4);
        room2.setPrice(1000);
        room2.setRating(5);
        rooms.add(room2);

        HotelRoom room3 = new HotelRoom();
        room3.setRoomNumber(3);
        room3.setStatus(HotelRoom.RoomStatus.SERVED);
        room3.setMaxPersonCount(4);
        room3.setPrice(500);
        room3.setRating(3);
        rooms.add(room3);

        HotelRoom room4 = new HotelRoom();
        room4.setRoomNumber(4);
        room4.setStatus(HotelRoom.RoomStatus.SERVED);
        room4.setMaxPersonCount(4);
        room4.setPrice(500);
        room4.setRating(5);
        rooms.add(room4);

        filteredRooms.add(room4);

        given(this.hotelRoomDao.getEmptyHotelRoomInPeriod(start, end)).willReturn(rooms);
        List<HotelRoom> returnedRooms = hotelRoomService.getRoomsByCriteria(start, end, 4, 600, 5);
        assertTrue(filteredRooms.size() == returnedRooms.size() &&
                filteredRooms.containsAll(returnedRooms) && returnedRooms.containsAll(filteredRooms));
    }

}
package com.hotel.administrator.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hotel.administrator.dto.NewBookingDto;
import com.hotel.administrator.service.BookingException;
import com.hotel.administrator.service.BookingService;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class AMQPListener {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BookingService bookingService;

    @SneakyThrows
    @RabbitListener(queues = {"${queue-sending-booking-name}"})
    public void onMessage(String message){
        NewBookingDto newBookingDto = objectMapper.readValue(message, NewBookingDto.class);
        try {
            bookingService.checkBooking(newBookingDto);
        } catch (BookingException e){
            log.warn("Ошибка в сервисе бронинрования", e);
        }
    }
}

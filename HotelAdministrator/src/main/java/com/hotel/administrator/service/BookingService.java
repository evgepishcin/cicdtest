package com.hotel.administrator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.hotel.administrator.dto.BookingDto;
import com.hotel.administrator.dto.NewBookingDto;
import com.hotel.administrator.model.BookingStatus;
import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.Person;
import com.hotel.administrator.repos.PersonDao;
import lombok.SneakyThrows;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private HotelService hotelService;

    @Autowired
    private HotelRoomService hotelRoomService;

    @Autowired
    private PersonDao personDao;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${fanout-ready-booking-name}")
    private String fanoutReadyBooking;

    @SneakyThrows
    @Transactional
    public void checkBooking(NewBookingDto newBookingDto) {
        BookingDto bookingDto = new BookingDto();
        bookingDto.setId(newBookingDto.getId());
        bookingDto.setStartDate(newBookingDto.getStartDate());
        bookingDto.setEndDate(newBookingDto.getEndDate());
        bookingDto.setHighPrice(newBookingDto.getHighPrice());
        bookingDto.setRequiredRating(newBookingDto.getRequiredRating());
        bookingDto.setRoomCapacity(newBookingDto.getRoomCapacity());
        try {
            List<HotelRoom> rooms = hotelRoomService.getRoomsByCriteria(newBookingDto.getStartDate(), newBookingDto.getEndDate(),
                    newBookingDto.getRoomCapacity(), newBookingDto.getHighPrice(), newBookingDto.getRequiredRating());
            if (rooms.size() == 0) {
                bookingDto.setBookingStatus(BookingStatus.DECLINED);
                StringWriter stringWriter = new StringWriter();
                objectMapper.writeValue(stringWriter, bookingDto);
                amqpTemplate.convertAndSend(fanoutReadyBooking, "", stringWriter.toString());
            } else {
                HotelRoom room = rooms.get(0);
                Person person = personDao.saveAndFlush(new Person());
                hotelService.settlePerson(person.getId(), room.getRoomNumber(),
                        newBookingDto.getStartDate(), newBookingDto.getEndDate());
                bookingDto.setBookingStatus(BookingStatus.CONFIRMED);
                StringWriter stringWriter = new StringWriter();
                objectMapper.writeValue(stringWriter, bookingDto);
                amqpTemplate.convertAndSend(fanoutReadyBooking, "", stringWriter.toString());
            }
        }catch (IOException | HotelServiceException e){
            bookingDto.setBookingStatus(BookingStatus.DECLINED);
            StringWriter stringWriter = new StringWriter();
            objectMapper.writeValue(stringWriter, bookingDto);
            amqpTemplate.convertAndSend(fanoutReadyBooking, "", stringWriter.toString());
            throw new BookingException("Не удалось обработать бронь", e);
        }
    }
}

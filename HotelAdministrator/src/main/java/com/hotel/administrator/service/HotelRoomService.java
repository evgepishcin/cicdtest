package com.hotel.administrator.service;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.repos.HotelRoomDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class HotelRoomService {

    @Autowired
    private HotelRoomDao hotelRoomDao;

    public List<HotelRoom> getRoomsByCriteria(LocalDateTime startDate, LocalDateTime endDate
            , Integer capacity, Integer price, Integer rating){
        List<HotelRoom> rooms = hotelRoomDao.getEmptyHotelRoomInPeriod(startDate, endDate);
        Stream<HotelRoom> roomStream = rooms.stream();
        if (capacity != null){
            roomStream = roomStream.filter(x -> x.getMaxPersonCount() >= capacity);
        }
        if (price != null){
            roomStream = roomStream.filter(x -> x.getPrice() <= price);
        }
        if (rating != null){
            roomStream = roomStream.filter(x -> x.getRating() >= rating);
        }
        return roomStream.collect(Collectors.toList());
    }
}

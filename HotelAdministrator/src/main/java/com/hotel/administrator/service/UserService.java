package com.hotel.administrator.service;

import com.hotel.administrator.model.Person;
import com.hotel.administrator.model.Status;
import com.hotel.administrator.model.User;
import com.hotel.administrator.repos.PersonDao;
import com.hotel.administrator.repos.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class UserService {

    private static final Logger log = LogManager.getLogger();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    @Autowired
    private PersonDao personDao;

    public void createUser(User user) {
        try {
            user.setId(0);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userDao.save(user);
        } catch (Exception e) {
            log.warn("Can't create new user", e);
            throw new HotelServiceException("Can't create new user", e);
        }
    }

    public void setStatus(int userId, Status status) {
        try {
            User user = userDao.findById(userId).orElse(null);
            user.setStatus(status);
            userDao.save(user);
        } catch (Exception e) {
            log.warn("Can't change user status", e);
            throw new HotelServiceException("Can't change user status", e);
        }
    }


    public void deleteUser(int userId) {
        try {
            userDao.deleteById(userId);
        } catch (Exception e) {
            log.warn("Can't delete user", e);
            throw new HotelServiceException("Can't delete user", e);
        }
    }

    public User getUser(int userId) {
        try {
            return userDao.findById(userId).orElse(null);
        } catch (Exception e) {
            log.warn("can't get user", e);
            throw new HotelServiceException("can't get user", e);
        }
    }

    public User getByName(String name){
        return userDao.findByLogin(name);
    }

    public List<User> getAllUsers() {
        try {
            return userDao.findAll();
        } catch (Exception e) {
            log.warn("can't get all users", e);
            throw new HotelServiceException("can't get all users", e);
        }
    }
}

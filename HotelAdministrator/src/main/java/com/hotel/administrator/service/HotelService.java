package com.hotel.administrator.service;

import com.hotel.administrator.model.*;
import com.hotel.administrator.repos.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;


@Component
public class HotelService {

    private static final Logger logger = LogManager.getLogger();


    @Autowired
    private HotelRoomDao hotelRoomDao;

    @Autowired
    private PersonDao personDao;

    @Autowired
    private RoomHistoryDao roomHistoryDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private ServiceHistoryDao serviceHistoryDao;

    @Value("${room-history-size-limit}")
    private int roomHistorySizeLimit = 3;

    @Value("${allow-change-room-status}")
    private boolean canChangeRoomStatus = true;


    public int getRoomHistorySizeLimit() {
        return roomHistorySizeLimit;
    }

    public void setRoomHistorySizeLimit(int roomHistorySizeLimit) {
        this.roomHistorySizeLimit = roomHistorySizeLimit;
    }

    @Transactional
    public void addHotelRoom(HotelRoom hotelRoom) {
        try {
            hotelRoomDao.save(hotelRoom);
            logger.info("В отеле построена новая комната с номером " + hotelRoom.getRoomNumber());
        } catch (Exception e) {
            logger.warn("не удеается построить комнату. Возможно комната с таким номеров уже существует", e);
            throw new HotelServiceException("не удеается построить комнату. Возможно комната с таким номеров уже существует", e);
        }
    }

    @Transactional
    public void registerGuest(Person person) {
        try {
            personDao.save(person);
            logger.info("Пользователь был добавлен в систему");
        } catch (Exception e) {
            logger.warn("Не удалось добавить пользователя в систему", e);
            throw new HotelServiceException("Не удалось добавить пользователя в систему", e);
        }
    }

    public List<Person> getPersons() {
        try {
            return personDao.findAll();
        } catch (Exception e) {
            logger.warn("Произошла ошибка при попытке получить всех гостей", e);
            throw new HotelServiceException("Произошла ошибка при попытке получить всех гостей", e);
        }
    }

    public Person getPersonById(int id) {
        try {
            return personDao.findById(id).orElse(null);
        } catch (Exception e) {
            logger.warn("Произошла ошибка при попытке получения гостя из базы данных", e);
            throw new HotelServiceException("Произошла ошибка при попытке получения гостя из базы данных", e);
        }
    }

    public List<HotelRoom> getRooms() {
        try {
            return hotelRoomDao.findAll();
        } catch (Exception e) {
            logger.warn("Произошла ошибка при попытке получить все номера", e);
            throw new HotelServiceException("Произошла ошибка при попытке получить все номера", e);
        }
    }

    public Service getServiceById(int id) {
        try {
            return serviceDao.findById(id).orElse(null);
        } catch (Exception e) {
            logger.warn("Произошла ошибка при получении сервиса из базы данных", e);
            throw new HotelServiceException("Произошла ошибка при получении сервиса из базы данных", e);
        }
    }

    public ServiceHistory getServiceHistoryById(int id) {
        try {
            return serviceHistoryDao.findById(id).orElse(null);
        } catch (Exception e) {
            logger.warn("Произошла ошибка при получении истории услуг из базы данных", e);
            throw new HotelServiceException("Произошла ошибка при получении истории услуг из базы данных", e);
        }
    }

    public List<Service> getServices() {
        try {
            return serviceDao.findAll();
        } catch (Exception e) {
            logger.warn("Произошла ошибка при попытке получить все услуги", e);
            throw new HotelServiceException("Произошла ошибка при попытке получить все услуги", e);
        }
    }

    public HotelRoom getHotelRoomByNumber(int number) {
        try {
            return hotelRoomDao.findById(number).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается получить комнату по номеру", e);
            throw new HotelServiceException("Не удается получить комнату по номеру", e);
        }
    }

    public HotelRoom getHotelRoomByPerson(int personId) {
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        try {
            return hotelRoomDao.getHotelRoomByPerson(personId, currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить комнату постояльца", e);
            throw new HotelServiceException("Не удается получить комнату постояльца", e);
        }
    }

    public List<HotelRoom> getEmptyHotelRooms() {
        try {
            LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return hotelRoomDao.getEmptyHotelRooms(currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить пустые комнаты", e);
            throw new HotelServiceException("Не удается получить пустые комнаты", e);
        }
    }

    public List<RoomHistory> getRoomHistoryRoomWillBeEmpty(LocalDateTime date) {
        try {
            return roomHistoryDao.getWillBeFree(date);
        } catch (Exception e) {
            logger.warn("Не удается получить список комнат, которые будут свободны", e);
            throw new HotelServiceException("Не удается получить список комнат, которые будут свободны", e);
        }
    }

    public int getEmptyHotelRoomCount() {
        return getEmptyHotelRooms().size();
    }

    public List<Person> getSettledPersons() {
        try {
            LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return personDao.getSettledPersons(currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить список заселенных людей", e);
            throw new HotelServiceException("Не удается получить список заселенных людей", e);
        }
    }

    public int getSettledPersonsCount() {
        return getSettledPersons().size();
    }


    @Transactional
    public void addService(Service service) {
        try {
            serviceDao.save(service);
            logger.info("Услуга была добавлена");
        } catch (Exception e) {
            logger.warn("Не удалось добавить услугу");
            throw new HotelServiceException("Не удалось добавить услугу", e);
        }
    }

    @Transactional
    public void changeServicePrice(int serviceId, int newPrice) {
        Service service;
        try {
            service = serviceDao.findById(serviceId).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается получить услугу по ее id", e);
            throw new HotelServiceException("Не удается получить услугу по ее id", e);
        }
        if (service == null) {
            logger.info("Не существует данной услуги");
            return;
        }
        service.setPrice(newPrice);
        try {
            serviceDao.save(service);
        } catch (Exception e) {
            logger.info("Не удается обновить цену услуги", e);
            throw new HotelServiceException("Не удается обновить цену услуги", e);
        }
    }

    @Transactional
    public void changeRoomPrice(int roomNumber, int newPrice) {
        HotelRoom hotelRoom;
        try {
            hotelRoom = hotelRoomDao.findById(roomNumber).orElse(null);
        } catch (Exception e) {
            logger.info("Не удается получить комнату по ее номеру", e);
            throw new HotelServiceException("Не удается получить комнату по ее номеру", e);
        }
        if (hotelRoom == null) {
            logger.info("Не существует данной комнаты");
            return;
        }
        hotelRoom.setPrice(newPrice);
        try {
            hotelRoomDao.save(hotelRoom);
        } catch (Exception e) {
            logger.warn("Не удается обновить комнату", e);
            throw new HotelServiceException("Не удается обновить комнату", e);
        }
    }


    @Transactional
    public void settlePerson(int personId, int roomNumber, LocalDateTime dateOfEviction){
        LocalDateTime startDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        settlePerson(personId, roomNumber, startDate, dateOfEviction);
    }

    @Transactional
    public void settlePerson(int personId, int roomNumber, LocalDateTime startDate, LocalDateTime dateOfEviction) {
        Person person;
        try {
            person = personDao.findById(personId).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается получить человека по его id", e);
            throw new HotelServiceException("Не удается получить человека по его id", e);
        }
        if (person == null) {
            logger.info("Данный гость не зарегестрирован в системе");
            return;
        }
        if (getHotelRoomByPerson(personId) == null) {
            if (startDate.compareTo(dateOfEviction) > 0) {
                logger.info("Дата выселения не может быть до даты заселения!");
                throw new HotelServiceException("Дата выселения не может быть до даты заселения!");
            }
            HotelRoom room;
            try {
                room = hotelRoomDao.findById(roomNumber).orElse(null);
            } catch (Exception e) {
                logger.warn("Не удается получить комнату по ее номеру", e);
                throw new HotelServiceException("Не удается получить комнату по ее номеру", e);
            }
            if (room == null) {
                logger.info("Невозможно заселить в номер " + roomNumber + ", поскольку такого номера не существует");
                throw new HotelServiceException("Невозможно заселить в номер "
                        + roomNumber + ", поскольку такого номера не существует");
            }
            if (room.getStatus().equals(HotelRoom.RoomStatus.SERVED)) {
                List<Person> roomPersons;
                try {
                    roomPersons = personDao.getHotelRoomPersons(roomNumber, startDate);
                } catch (Exception e) {
                    logger.warn("Не получается получить постояльцев, заселенных в комнате " + roomNumber, e);
                    throw new HotelServiceException("Не получается получить постояльцев, заселенных в комнате " + roomNumber, e);
                }
                if (roomPersons.size() == room.getMaxPersonCount()) {
                    logger.warn("\"Заселение в номер не выполнено, так как в номере закончились места");
                } else {
                    RoomHistory roomHistory = new RoomHistory();
                    roomHistory.setPerson(person);
                    roomHistory.setHotelRoom(room);
                    roomHistory.setStartDate(startDate);
                    roomHistory.setEndDate(dateOfEviction);
                    try {
                        roomHistoryDao.save(roomHistory);
                        roomHistoryDao.deleteOldRecords(roomNumber, roomHistorySizeLimit, startDate);
                        logger.info("Гость был заселен");
                    } catch (Exception e) {
                        logger.warn("не удалось заселить человека", e);
                        throw new HotelServiceException("не удалось заселить человека", e);
                    }
                }
            } else {
                logger.info("Заселение в номер не выполнено, так как номер на ремонте");
                throw new HotelServiceException("Заселение в номер не выполнено, так как номер на ремонте");
            }
        } else {
            logger.info("Поселение в номер не выполнено, так как данный человек уже заселен");
            throw new HotelServiceException("Поселение в номер не выполнено, так как данный человек уже заселен");
        }
    }

    @Transactional
    public void evictPerson(int personId) {
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        RoomHistory roomHistory;
        try {
            roomHistory = roomHistoryDao.getRoomHistoryBySettledPerson(personId, currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить историю заселения постояльца", e);
            throw new HotelServiceException("Не удается получить историю заселения постояльца", e);
        }
        if (roomHistory != null) {
            roomHistory.setEndDate(currentDate);
            try {
                roomHistoryDao.save(roomHistory);
                roomHistoryDao.deleteOldRecords(roomHistory.getHotelRoom().getRoomNumber(), roomHistorySizeLimit, currentDate);
                logger.info("Гость был выселен");
            } catch (Exception e) {
                logger.warn("Произошла ошибка при попытке выселения гостя", e);
                throw new HotelServiceException("Произошла ошибка при попытке выселения гостя", e);
            }
        } else {
            logger.info("Данный гость не заселен");
        }
    }

    @Transactional
    public void setRoomStatus(int roomNumber, HotelRoom.RoomStatus status) {
        HotelRoom hotelRoom;
        try {
            hotelRoom = hotelRoomDao.findById(roomNumber).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удалось получить комнату по ее номеру", e);
            throw new HotelServiceException("Не удалось получить комнату по ее номеру", e);
        }
        if (hotelRoom == null) {
            logger.info("Не существует такого номера");
            return;
        }
        if (!canChangeRoomStatus) {
            logger.info("Установлен запрет на изменение статуса номера!");
            return;
        }
        if (status == HotelRoom.RoomStatus.REPAIRED) {
            LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            List<Person> people;
            try {
                people = personDao.getHotelRoomPersons(roomNumber, currentDate);
            } catch (Exception e) {
                logger.warn("Не удается получить постояльцев, проживающих в номере " + roomNumber, e);
                throw new HotelServiceException("Не удается получить постояльцев, проживающих в номере " + roomNumber, e);
            }
            if (people.isEmpty()) {
                hotelRoom.setStatus(status);
                try {
                    hotelRoomDao.save(hotelRoom);
                } catch (Exception e) {
                    logger.warn("Не удается обновить номер", e);
                    throw new HotelServiceException("Не удается обновить номер", e);
                }
                logger.info("Статус номера установлен на \"Ремонт\"");
            } else {
                logger.info("Не возможно изменить статус номера " +
                        "на ремонт, так как в номере есть люди");
                throw new HotelServiceException("Не возможно изменить статус номера " +
                        "на ремонт, так как в номере есть люди");
            }
        } else {
            hotelRoom.setStatus(status);
            try {
                hotelRoomDao.save(hotelRoom);
            } catch (Exception e) {
                logger.warn("Не удается обновить номер");
                throw new HotelServiceException("Не удается обновить номер", e);
            }
            logger.info("Статус номера установлен на \"Обслуживаемый\"");
        }

    }


    @Transactional
    public void addServiceToPerson(int personId, int serviceId, LocalDateTime date) {
        RoomHistory roomHistory;
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        try {
            roomHistory = roomHistoryDao.getRoomHistoryBySettledPerson(personId, currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить историю поселения в номер постояльца " + personId, e);
            throw new HotelServiceException("Не удается получить историю поселения в номер постояльца " + personId, e);
        }
        if (roomHistory == null) {
            logger.info("Невозможно добавить услугу гостю, так как он не заселен");
            return;
        }
        if (date.isBefore(currentDate)) {
            logger.info("Невозможно добавить услугу на прошедшую дату!");
            return;
        }

        if (roomHistory.getEndDate().isBefore(date)) {
            logger.info("Невозможно добавить услугу после даты выселения гостя!");
            return;
        }
        Person person;
        try {
            person = personDao.findById(personId).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается найти постояльца по его id", e);
            throw new HotelServiceException("Не удается найти постояльца по его id", e);
        }
        Service service;
        try {
            service = serviceDao.findById(serviceId).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается найти сервис по его id", e);
            throw new HotelServiceException("Не удается найти сервис по его id", e);
        }
        ServiceHistory serviceHistory = new ServiceHistory();
        serviceHistory.setPerson(person);
        serviceHistory.setService(service);
        serviceHistory.setDate(date);
        try {
            serviceHistoryDao.save(serviceHistory);
            logger.info("Услуга была добавлена");
        } catch (Exception e) {
            logger.info("Услуга не была добавлена. Возможно нет такой услуги или гостя", e);
            throw new HotelServiceException("Услуга не была добавлена. Возможно нет такой услуги или гостя", e);
        }
    }

    public int getPersonPriceForHotelRoom(Integer personId) {
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime lastDate;
        RoomHistory roomHistory;
        try {
            roomHistory = roomHistoryDao.getLastRoomHistoryByPersonId(personId);
        } catch (Exception e) {
            logger.warn("Не удается получить историю поселения для пользователя " + personId, e);
            throw new HotelServiceException("Не удается получить историю поселения для пользователя " + personId, e);
        }
        if (roomHistory == null) {
            return 0;
        }
        HotelRoom hotelRoom;
        try {
            hotelRoom = hotelRoomDao.findById(roomHistory.getHotelRoom().getRoomNumber()).orElse(null);
        } catch (Exception e) {
            logger.warn("Не удается получить комнату, которая указана в истории постояльца " + personId, e);
            throw new HotelServiceException("Не удается получить комнату, которая указана в истории постояльца " + personId, e);
        }
        if (hotelRoom == null) {
            return 0;
        }
        if (currentDate.isBefore(roomHistory.getEndDate())) {
            lastDate = currentDate;
        } else {
            lastDate = roomHistory.getEndDate();
        }
        int days = (int) Duration.between(roomHistory.getStartDate(), lastDate).toDays();
        return (days + 1) * hotelRoom.getPrice();
    }
}

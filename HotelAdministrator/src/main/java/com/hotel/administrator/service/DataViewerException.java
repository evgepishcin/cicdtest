package com.hotel.administrator.service;

public class DataViewerException extends RuntimeException {
    public DataViewerException(String message) {
        super(message);
    }

    public DataViewerException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.hotel.administrator.service;

import com.hotel.administrator.comparators.*;
import com.hotel.administrator.dto.HotelRoomDto;
import com.hotel.administrator.dto.RoomHistoryDto;
import com.hotel.administrator.dto.ServiceDto;
import com.hotel.administrator.dto.ServiceHistoryDto;
import com.hotel.administrator.model.*;
import com.hotel.administrator.repos.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataViewer {

    private static final Logger logger = LogManager.getLogger(DataViewer.class);

    @Autowired
    RoomHistoryDao roomHistoryDao;

    @Autowired
    ServiceHistoryDao serviceHistoryDao;

    @Autowired
    HotelService hotelService;

    @Autowired
    HotelRoomDao hotelRoomDao;

    @Autowired
    PersonDao personDao;

    @Autowired
    ServiceDao serviceDao;

    public List<RoomHistoryDto> showLastQuests(int roomNumber, int count) {
        List<RoomHistory> roomHistories;
        try {
            roomHistories = roomHistoryDao.getRoomHistoryByRoomNumber(roomNumber);
        } catch (Exception e) {
            logger.warn("Не удается получить истории заселения в номер " + roomNumber, e);
            throw new DataViewerException("Не удается получить истории заселения в номер " + roomNumber);
        }
        int size = count;
        if (count > roomHistories.size()) {
            size = roomHistories.size();
        }
        List<RoomHistoryDto> dto = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            RoomHistory roomHistory = roomHistories.get(i);
            dto.add(new RoomHistoryDto(roomHistory));
        }
        return dto;
    }

    public List<HotelRoomDto> showRoomList(SortType sortType) {
        List<HotelRoom> rooms = hotelService.getRooms();
        return showRooms(rooms, sortType);
    }

    public List<HotelRoomDto> showEmptyRoomList(SortType sortType) {
        List<HotelRoom> rooms = hotelService.getEmptyHotelRooms();
        return showRooms(rooms, sortType);
    }

    private List<HotelRoomDto> showRooms(List<HotelRoom> rooms, SortType sortType) {
        List<HotelRoom> sortRooms = new ArrayList<>(rooms);
        switch (sortType) {
            case PRICE:
                sortRooms.sort(new PayableComparator());
                break;
            case CAPACITY:
                Collections.sort(sortRooms);
                break;
            case RATING:
                sortRooms.sort(new StarRatingComparator());
                break;
            default:
                logger.warn("Данный вид сортировки не поддерживается для текущего метода");
                throw new DataViewerException("Данный вид сортировки не поддерживается для текущего метода");
        }
        return sortRooms
                .stream()
                .map(HotelRoomDto::new)
                .collect(Collectors.toList());
    }

    public List<Payable> showPayables(SortType sortType) {
        List<HotelRoom> rooms;
        try {
            rooms = hotelRoomDao.findAll();
        } catch (Exception e) {
            logger.warn("Не удается получить список всех комнат", e);
            throw new DataViewerException("Не удается получить список всех комнат", e);
        }
        List<Service> services;
        try {
            services = serviceDao.findAll();
        } catch (Exception e) {
            logger.warn("Не удается получить список всех услуг", e);
            throw new DataViewerException("Не удается получить список всех услуг", e);
        }
        List<Payable> payables = new ArrayList<>();
        switch (sortType) {
            case PRICE:
                payables.addAll(rooms);
                payables.addAll(services);
                payables.sort(new PayableComparator());
                break;
            case SECTION:
                services.sort(new ServiceComparatorTitle());
                rooms.sort(new HotelRoomComparatorRoomNumber());
                payables.addAll(services);
                payables.addAll(rooms);
                break;
            default:
                logger.warn("Данный вид сортировки не поддерживается для текущего метода");
                return new ArrayList<>();
        }
        List<Payable> dtos = new ArrayList<>();
        for (Payable payable : payables) {
            if (payable instanceof HotelRoom) {
                dtos.add(new HotelRoomDto((HotelRoom) payable));
            } else if (payable instanceof Service) {
                dtos.add(new ServiceDto((Service) payable));
            }
        }
        return dtos;
    }

    public List<ServiceHistoryDto> showPersonService(int personId, SortType sortType) {
        boolean sortDate;
        switch (sortType) {
            case PRICE:
                sortDate = false;
                break;
            case DATE:
                sortDate = true;
                break;
            default:
                logger.warn("Данный вид сортировки не поддерживается для текущего метода");
                return new ArrayList<>();
        }
        List<ServiceHistory> serviceHistories;
        try {
            serviceHistories = serviceHistoryDao.getServiceHistoryByPersonId(personId);
        } catch (Exception e) {
            logger.warn("Не удается получить услуги пользователя", e);
            throw new DataViewerException("Не удается получить услуги пользователя");
        }
        if (sortDate) {
            serviceHistories.sort(new ServiceHistoryComparatorDate());
        } else {
            serviceHistories.sort(new ServiceHistoryComparatorPrice());
        }
        return serviceHistories
                .stream()
                .map(ServiceHistoryDto::new)
                .collect(Collectors.toList());
    }

    public List<RoomHistory> showPersonsRoomList(SortType sortType) {
        switch (sortType) {
            case ALPHABET:
                return personsRoomListAlphabet();
            case END_DATE:
                return personsRoomListDateOfEviction();
            default:
                logger.warn("Данный вид сортировки не поддерживается для текущего метода");
                throw new DataViewerException("Данный вид сортировки не поддерживается для текущего метода");
        }
    }

    private List<RoomHistory> personsRoomListAlphabet() {
        List<RoomHistory> roomHistories = new ArrayList<>();
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        List<Person> persons;
        try {
            persons = personDao.getSettledPersons(currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить список заселенных постояльцев", e);
            throw new DataViewerException("Не удается получить список заселенных постояльцев");
        }
        persons.sort(new PersonComparatorAlphabet());
        for (Person person : persons) {
            RoomHistory roomHistory;
            try {
                roomHistory = roomHistoryDao.getLastRoomHistoryByPersonId(person.getId());
            } catch (Exception e) {
                logger.warn("Не удается получить последнюю комнату," +
                        " в которой проживал постоялец " + person.getId(), e);
                throw new DataViewerException("Не удается получить последнюю комнату," +
                        " в которой проживал постоялец " + person.getId());
            }
            roomHistory.getPerson().setRoomHistories(null);
            roomHistory.getHotelRoom().setRoomHistories(null);
            roomHistories.add(roomHistory);
        }
        return roomHistories;
    }

    private List<RoomHistory> personsRoomListDateOfEviction() {
        List<RoomHistory> roomHistories = new ArrayList<>();
        LocalDateTime currentDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        List<Person> persons;
        try {
            persons = personDao.getSettledPersons(currentDate);
        } catch (Exception e) {
            logger.warn("Не удается получить заселенных постояльцев", e);
            throw new DataViewerException("Не удается получить заселенных постояльцев");
        }
        for (Person person : persons) {
            RoomHistory roomHistory;
            try {
                roomHistory = roomHistoryDao.getLastRoomHistoryByPersonId(person.getId());
            } catch (Exception e) {
                logger.warn("Не удается получить историю заселения постояльца " + person.getId(), e);
                throw new DataViewerException("Не удается получить историю заселения постояльца " + person.getId());
            }
            roomHistory.getHotelRoom().setRoomHistories(null);
            roomHistory.getPerson().setRoomHistories(null);
            roomHistories.add(roomHistory);
        }
        roomHistories.sort(new RoomHistoryComparatorEndDate());
        return roomHistories;
    }

    public enum SortType {
        PRICE,
        CAPACITY,
        RATING,
        ALPHABET,
        DATE,
        END_DATE,
        SECTION
    }
}

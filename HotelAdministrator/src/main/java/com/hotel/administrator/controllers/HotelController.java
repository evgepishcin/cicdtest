package com.hotel.administrator.controllers;

import com.hotel.administrator.model.*;
import com.hotel.administrator.repos.RoomHistoryDao;
import com.hotel.administrator.service.DataViewer;
import com.hotel.administrator.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @Autowired
    private DataViewer dataViewer;

    @Autowired
    private RoomHistoryDao roomHistoryDao;

    /**
     * Добавление услуги постояльцу отеля
     *
     * @param personId  - id постояльца
     * @param serviceId - id добавляемой услуги
     * @param day       - день оказания услуги
     * @param month     - месяц оказания услуги
     * @param year      - год оказания услуги
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/addServiceToPerson")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> addServiceToPerson(@RequestParam int personId, @RequestParam
            int serviceId, @RequestParam int day, @RequestParam int month, @RequestParam int year) {
        Person person = hotelService.getPersonById(personId);
        Service service = hotelService.getServiceById(serviceId);
        if (person == null || service == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            LocalDateTime date = LocalDateTime.of(year, month, day, 0, 0);
            hotelService.addServiceToPerson(personId, serviceId, date);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    /**
     * Заселение пользователя в гостиничный номер
     *
     * @param personId - id заселяемого пользователя
     * @param number   - номер комнаты для заселения
     * @param day      - день выселения из номера
     * @param month    - месяц выселения из номера
     * @param year     - год выселения из номера
     * @param hour     - час выселения из номера
     * @param minute   - минута выселения из номера
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/settlePerson")
    @PreAuthorize("hasAuthority('" + Permission.SETTLE_PERSON + "')")
    public ResponseEntity<HttpStatus> settlePerson(
            @RequestParam int personId, @RequestParam int number,
            @RequestParam int day, @RequestParam int month, @RequestParam int year,
            @RequestParam int hour, @RequestParam int minute
    ) {
        LocalDateTime date = LocalDateTime.of(year, month, day, hour, minute);
        hotelService.settlePerson(personId, number, date);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Досрочное выселение постояльца из номера
     *
     * @param personId - id выселяемого постояльца
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/evictPerson")
    @PreAuthorize("hasAuthority('" + Permission.EVICT_PERSON + "')")
    public ResponseEntity<HttpStatus> evictPerson(@RequestParam int personId) {
        hotelService.evictPerson(personId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Получение списка номеров и услуг
     *
     * @param sortType - вид сортировки списка (по разделу или цене)
     * @return - список номеров и услуг
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/showPayables")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<Payable>> showPayables(@RequestParam String sortType) {
        List<Payable> dtos;
        if (sortType.equals("price")) {
            dtos = dataViewer.showPayables(DataViewer.SortType.PRICE);
        } else if (sortType.equals("section")) {
            dtos = dataViewer.showPayables(DataViewer.SortType.SECTION);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Получения списка заселений постояльцев
     *
     * @param sortType - вид сортировки (по ФИО постояльца или по дате выселения из номера)
     * @return - список заселения постояльцев
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/personsRoom")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<RoomHistory>> personsRoom(@RequestParam String sortType) {
        List<RoomHistory> roomHistories;
        if (sortType.equals("alphabet")) {
            roomHistories = dataViewer.showPersonsRoomList(DataViewer.SortType.ALPHABET);
        } else if (sortType.equals("date")) {
            roomHistories = dataViewer.showPersonsRoomList(DataViewer.SortType.END_DATE);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(roomHistories, HttpStatus.OK);
    }

}

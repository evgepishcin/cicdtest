package com.hotel.administrator.controllers;

import com.hotel.administrator.dto.ServiceHistoryDto;
import com.hotel.administrator.model.Permission;
import com.hotel.administrator.model.ServiceHistory;
import com.hotel.administrator.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceHistoryController {

    @Autowired
    private HotelService hotelService;

    /**
     * Получение истории оказания услуги
     *
     * @param id - id услуги
     * @return - услуга
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/serviceHistory/{id:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<ServiceHistoryDto> getServiceHistory(@PathVariable int id) {
        ServiceHistory serviceHistory = hotelService.getServiceHistoryById(id);
        if (serviceHistory == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            ServiceHistoryDto dto = new ServiceHistoryDto(serviceHistory);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
    }
}

package com.hotel.administrator.controllers;

import com.hotel.administrator.dto.PersonDto;
import com.hotel.administrator.dto.ServiceHistoryDto;
import com.hotel.administrator.model.Permission;
import com.hotel.administrator.model.Person;
import com.hotel.administrator.service.DataViewer;
import com.hotel.administrator.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
public class PersonController {

    @Autowired
    private HotelService hotelService;

    @Autowired
    private DataViewer dataViewer;

    /**
     * Получение пользователя
     *
     * @param id - id пользователя
     * @return - пользователь
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/person/{id:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<PersonDto> getPerson(@PathVariable int id) {
        Person person = null;
        person = hotelService.getPersonById(id);
        if (person == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            PersonDto personDto = new PersonDto(person);
            return new ResponseEntity<>(personDto, HttpStatus.OK);
        }
    }

    /**
     * Регистрация пользователя в системе
     *
     * @param personDto - новый пользователь
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("person/register")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> registration(@RequestBody PersonDto personDto) {
        Person person = personDto.toPerson();
        person.setId(0);
        hotelService.registerGuest(person);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Получение всех пользовтелей, зарегистрированных в системе
     *
     * @return - список всех пользовтелей, зарегистрированных в системе
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/person/all")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<PersonDto>> allPersons() {
        List<Person> services = hotelService.getPersons();
        List<PersonDto> dtos = services
                .stream()
                .map(PersonDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Постояльцы отеля
     *
     * @return - список постояльцев, которые заселены в отеле
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("person/settled")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<PersonDto>> settledPerson() {
        List<Person> people = hotelService.getSettledPersons();
        List<PersonDto> settled = people
                .stream()
                .map(PersonDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(settled, HttpStatus.OK);
    }

    /**
     * Количество постояльцев отеля
     *
     * @return - кол-во постояльцев отеля
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("person/settledCount")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<Integer> personCount() {
        Integer count = hotelService.getSettledPersonsCount();
        return new ResponseEntity<>(count, HttpStatus.OK);
    }

    /**
     * Список услуг постояльца
     *
     * @param id       - id постояльа отеля
     * @param sortType - вид сортировки (по цене или дате)
     * @return - список услуг постояльца
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("person/{id:\\d+}/services")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<ServiceHistoryDto>> showPersonService(
            @PathVariable int id, @RequestParam String sortType) {
        List<ServiceHistoryDto> dtos;
        if (sortType.equals("price")) {
            dtos = dataViewer.showPersonService(id, DataViewer.SortType.PRICE);
        } else if (sortType.equals("date")) {
            dtos = dataViewer.showPersonService(id, DataViewer.SortType.DATE);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Плата за номер постояльца
     *
     * @param id - id постояльца
     * @return - цена, которую необходимо заплатить постояльцу за номер
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("person/{id:\\d+}/roomPrice")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<Integer> personRoomPrice(@PathVariable int id) {
        Integer price = hotelService.getPersonPriceForHotelRoom(id);
        return new ResponseEntity<>(price, HttpStatus.OK);
    }
}

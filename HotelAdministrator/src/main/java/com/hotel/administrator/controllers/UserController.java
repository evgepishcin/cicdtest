package com.hotel.administrator.controllers;

import com.hotel.administrator.dto.AuthenticationRequestDto;
import com.hotel.administrator.dto.PersonDto;
import com.hotel.administrator.dto.UserWithoutPasswordDto;
import com.hotel.administrator.model.*;
import com.hotel.administrator.security.SecurityUser;
import com.hotel.administrator.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/create")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AuthenticationRequestDto dto){
        User user = new User();
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());
        user.setRole(Role.USER);
        user.setStatus(Status.ENABLED);
        userService.createUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/{id:\\d+}/delete")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<?> deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("/{id:\\d+}/changeStatus")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<?> changeStatus(@PathVariable int id, @RequestParam Status status) {
        userService.setStatus(id, status);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/{id:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<UserWithoutPasswordDto> getUser(@PathVariable int id) {
        User user = userService.getUser(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            UserWithoutPasswordDto userWp = new UserWithoutPasswordDto(user);
            return new ResponseEntity<>(userWp, HttpStatus.OK);
        }
    }

    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('" + Permission.CRUD_USERS + "')")
    public ResponseEntity<List<UserWithoutPasswordDto>> getAllUsers() {
        List<UserWithoutPasswordDto> users = userService.getAllUsers()
                .stream()
                .map(UserWithoutPasswordDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}

package com.hotel.administrator.controllers;

import com.hotel.administrator.dto.HotelRoomDto;
import com.hotel.administrator.dto.RoomHistoryDto;
import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.Permission;
import com.hotel.administrator.model.RoomHistory;
import com.hotel.administrator.service.DataViewer;
import com.hotel.administrator.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class HotelRoomController {

    @Autowired
    HotelService hotelService;

    @Autowired
    DataViewer dataViewer;

    /**
     * Получение комнаты
     *
     * @param number - номер комнаты
     * @return - запрашиваемая комната
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/room/{number:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<HotelRoomDto> getRoom(@PathVariable int number) {
        HotelRoom hotelRoom = hotelService.getHotelRoomByNumber(number);
        if (hotelRoom == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            HotelRoomDto dto = new HotelRoomDto(hotelRoom);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
    }

    /**
     * Добавление новой комнаты в отель
     *
     * @param dto - добавляемая комната
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("room/add")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> addRoom(@RequestBody HotelRoomDto dto) {
        HotelRoom hotelRoom = dto.toHotelRoom();
        hotelService.addHotelRoom(hotelRoom);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Ищменение статуса комнаты
     *
     * @param number - номер комнаты, у которой следует изменить статус
     * @param status - статус номера (на ремонте, или на обслуживании)
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("room/changeStatus")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> changeRoomStatus(@RequestParam int number, @RequestParam String status) {
        HotelRoom.RoomStatus enumStatus;
        if (status.equals("SERVED")) {
            enumStatus = HotelRoom.RoomStatus.SERVED;
        } else {
            enumStatus = HotelRoom.RoomStatus.REPAIRED;
        }
        hotelService.setRoomStatus(number, enumStatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Изменение цены комнаты
     *
     * @param number   - номер комнаты, у которой следует изменить цену
     * @param newPrice - новая цена комнаты
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("room/changePrice")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> changeRoomPrice(@RequestParam int number, @RequestParam int newPrice) {
        hotelService.changeRoomPrice(number, newPrice);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Получение количества пустых комнат
     *
     * @return - кол-во пустых комнат
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("room/emptyRoomCount")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<Integer> emptyRoomCount() {
        int count = hotelService.getEmptyHotelRoomCount();
        return new ResponseEntity<>(count, HttpStatus.OK);
    }

    /**
     * Показать последних постояльцев номера
     *
     * @param number - номер комнаты
     * @param count  - количество записей, которые необходимо отобразить
     * @return - список истории поселения последних постояльцев номера
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/room/{number:\\d+}/showLastQuests")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<RoomHistoryDto>> showLastQuests(@PathVariable int number, @RequestParam int count) {
        List<RoomHistoryDto> dtos = dataViewer.showLastQuests(number, count);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Показать все номера отеля
     *
     * @param sortType - вид сортировки (по цене, рейтингу или вместимости номера)
     * @return - сортированный список номеров отеля
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("room/all")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<HotelRoomDto>> showRoomList(@RequestParam String sortType) {
        List<HotelRoomDto> dtos;
        if (sortType.equals("price")) {
            dtos = dataViewer.showRoomList(DataViewer.SortType.PRICE);
        } else if (sortType.equals("rating")) {
            dtos = dataViewer.showRoomList(DataViewer.SortType.RATING);
        } else if (sortType.equals("capacity")) {
            dtos = dataViewer.showRoomList(DataViewer.SortType.CAPACITY);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Показать все пустые номера отеля
     *
     * @param sortType - вид сортировки (по цене, рейтингу или вместимости номера
     * @return - сортированный список пустых номеров отеля
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("room/empty")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<HotelRoomDto>> showEmptyRoomList(@RequestParam String sortType) {
        List<HotelRoomDto> dtos;
        if (sortType.equals("price")) {
            dtos = dataViewer.showEmptyRoomList(DataViewer.SortType.PRICE);
        } else if (sortType.equals("rating")) {
            dtos = dataViewer.showEmptyRoomList(DataViewer.SortType.RATING);
        } else if (sortType.equals("capacity")) {
            dtos = dataViewer.showEmptyRoomList(DataViewer.SortType.CAPACITY);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Список номеров, которые будут свободны после определенной даты
     *
     * @param day    - день
     * @param month  - месяц
     * @param year   - год
     * @param hour   - час
     * @param minute - минута
     * @return - список номеров, которые будут свободны после указанной даты
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("room/willBeEmpty")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<RoomHistoryDto>> showWillBeEmptyRoomList(
            @RequestParam int day, @RequestParam int month, @RequestParam int year,
            @RequestParam int hour, @RequestParam int minute
    ) {
        LocalDateTime currentDate = LocalDateTime.of(year, month, day, hour, minute);
        List<RoomHistory> rooms = hotelService.getRoomHistoryRoomWillBeEmpty(currentDate);
        List<RoomHistoryDto> dtos = rooms
                .stream()
                .map(RoomHistoryDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}

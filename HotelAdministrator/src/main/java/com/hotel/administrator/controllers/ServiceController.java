package com.hotel.administrator.controllers;

import com.hotel.administrator.dto.ServiceDto;
import com.hotel.administrator.model.Permission;
import com.hotel.administrator.model.Service;
import com.hotel.administrator.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ServiceController {

    @Autowired
    private HotelService hotelService;

    /**
     * Получение услуги
     *
     * @param id - id услуги
     * @return - указанная услуга
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/service/{id:\\d+}")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<ServiceDto> getService(@PathVariable int id) {
        Service service = hotelService.getServiceById(id);
        if (service == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            ServiceDto serviceDto = new ServiceDto(service);
            return new ResponseEntity<>(serviceDto, HttpStatus.OK);
        }
    }

    /**
     * Получение всех услуг предлагаемых в отеле
     *
     * @return - список всех услуг предлагаемых в отеле
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @GetMapping("/service/all")
    @PreAuthorize("hasAuthority('" + Permission.READ_DATA + "')")
    public ResponseEntity<List<ServiceDto>> allServices() {
        List<Service> services = hotelService.getServices();
        List<ServiceDto> serviceDtos = services
                .stream()
                .map(ServiceDto::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(serviceDtos, HttpStatus.OK);
    }

    /**
     * Добавление новой услуги
     *
     * @param dto - добавляемая услуга
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("service/add")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> addService(@RequestBody ServiceDto dto) {
        Service service = dto.toService();
        service.setId(0);
        hotelService.addService(service);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Изменение цены услуги
     *
     * @param id       - id услуги
     * @param newPrice - новая цена услуги
     * @return - статус операции
     */
    @Operation(security = { @SecurityRequirement(name = "bearer-key") })
    @PostMapping("service/changePrice")
    @PreAuthorize("hasAuthority('" + Permission.WRITE_DATA + "')")
    public ResponseEntity<HttpStatus> changeServicePrice(@RequestParam int id, @RequestParam int newPrice) {
        hotelService.changeServicePrice(id, newPrice);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

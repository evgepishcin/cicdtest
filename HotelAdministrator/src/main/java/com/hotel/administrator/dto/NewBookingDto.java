package com.hotel.administrator.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class NewBookingDto {

    private Integer id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer roomCapacity;
    private Integer highPrice;
    private Integer requiredRating;

}

package com.hotel.administrator.dto;

import com.hotel.administrator.model.Role;
import com.hotel.administrator.model.Status;
import com.hotel.administrator.model.User;
import lombok.Data;

@Data
public class UserWithoutPasswordDto {

    private Integer id;
    private String login;
    private String role;
    private String status;

    public UserWithoutPasswordDto() {
    }

    public UserWithoutPasswordDto(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        if (user.getRole().equals(Role.ADMIN)) {
            this.role = "ADMIN";
        } else {
            this.role = "USER";
        }
        if (user.getStatus().equals(Status.ENABLED)) {
            this.status = "ENABLED";
        } else {
            this.status = "DISABLED";
        }
    }
}

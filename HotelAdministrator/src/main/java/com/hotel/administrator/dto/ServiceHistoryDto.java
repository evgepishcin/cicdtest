package com.hotel.administrator.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hotel.administrator.model.ServiceHistory;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ServiceHistoryDto {

    private Integer id;
    private Integer serviceId;
    private Integer personId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;

    public ServiceHistoryDto() {
    }

    public ServiceHistoryDto(ServiceHistory serviceHistory) {
        id = serviceHistory.getId();
        serviceId = serviceHistory.getService().getId();
        personId = serviceHistory.getPerson().getId();
        date = serviceHistory.getDate();
    }

}

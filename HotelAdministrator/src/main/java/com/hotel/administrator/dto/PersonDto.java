package com.hotel.administrator.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.hotel.administrator.model.Person;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonDto {

    protected Integer id = 0;

    protected String name = null;

    protected String surname = null;

    protected String patronymic = null;

    @JsonFormat(pattern="yyyy-MM-dd")
    protected LocalDate dateOfBirth = null;

    protected String phone = null;

    protected String email = null;

    public PersonDto() {
    }

    public PersonDto(Person person) {
        id = person.getId();
        name = person.getName();
        surname = person.getSurname();
        patronymic = person.getPatronymic();
        dateOfBirth = person.getDateOfBirth();
        phone = person.getPhone();
        email = person.getEmail();
    }

    public Person toPerson() {
        Person person = new Person();
        person.setId(id);
        person.setName(name);
        person.setSurname(surname);
        person.setPatronymic(patronymic);
        person.setDateOfBirth(dateOfBirth);
        person.setPhone(phone);
        person.setEmail(email);
        return person;
    }
}

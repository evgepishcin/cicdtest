package com.hotel.administrator.dto;

import com.hotel.administrator.model.BookingStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class BookingDto {

    private Integer id;
    private BookingStatus bookingStatus;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer roomCapacity;
    private Integer highPrice;
    private Integer requiredRating;

}

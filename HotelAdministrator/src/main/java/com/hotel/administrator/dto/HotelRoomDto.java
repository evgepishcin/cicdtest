package com.hotel.administrator.dto;

import com.hotel.administrator.model.HotelRoom;
import com.hotel.administrator.model.StarRating;
import lombok.Data;

@Data
public class HotelRoomDto extends StarRating {
    private Integer roomNumber = 0; //Так же играет роль id
    private Integer maxPersonCount;
    private HotelRoom.RoomStatus status = HotelRoom.RoomStatus.SERVED;

    public HotelRoomDto() {
    }

    public HotelRoomDto(HotelRoom hotelRoom) {
        roomNumber = hotelRoom.getRoomNumber();
        maxPersonCount = hotelRoom.getMaxPersonCount();
        status = hotelRoom.getStatus();
        starCount = hotelRoom.getRating();
        price = hotelRoom.getPrice();
    }

    public HotelRoom toHotelRoom() {
        HotelRoom hotelRoom = new HotelRoom();
        hotelRoom.setRoomNumber(roomNumber);
        hotelRoom.setMaxPersonCount(maxPersonCount);
        hotelRoom.setStatus(status);
        hotelRoom.setRating(starCount);
        hotelRoom.setPrice(price);
        return hotelRoom;
    }

}

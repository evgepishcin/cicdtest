package com.hotel.administrator.dto;

import com.hotel.administrator.model.Payable;
import com.hotel.administrator.model.Service;
import lombok.Data;

@Data
public class ServiceDto extends Payable {

    private Integer id;
    private String title;
    private String about;

    public ServiceDto() {
    }

    public ServiceDto(Service service) {
        id = service.getId();
        title = service.getTitle();
        about = service.getAbout();
        price = service.getPrice();
    }

    public Service toService() {
        Service service = new Service();
        service.setId(id);
        service.setTitle(title);
        service.setAbout(about);
        service.setPrice(price);
        return service;
    }

}

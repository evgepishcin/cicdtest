package com.hotel.administrator.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hotel.administrator.model.RoomHistory;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RoomHistoryDto {
    private Integer id;
    private Integer roomNumber;
    private Integer personId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    public RoomHistoryDto() {
    }

    public RoomHistoryDto(RoomHistory roomHistory) {
        id = roomHistory.getId();
        roomNumber = roomHistory.getHotelRoom().getRoomNumber();
        personId = roomHistory.getPerson().getId();
        startDate = roomHistory.getStartDate();
        endDate = roomHistory.getEndDate();
    }
}

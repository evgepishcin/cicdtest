package com.hotel.administrator.util;

import com.hotel.administrator.security.JwtAuthenticationException;
import com.hotel.administrator.service.BookingException;
import com.hotel.administrator.service.DataViewerException;
import com.hotel.administrator.service.HotelServiceException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@Log4j2
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HotelServiceException.class)
    public ResponseEntity<Object> handleHotelServiceErrors(HotelServiceException exception, WebRequest request) {
        log.warn("Произошла ошибка в сервисе отеля", exception);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BookingException.class)
    public ResponseEntity<Object> handleBookingException(BookingException exception, WebRequest request){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(DataViewerException.class)
    public ResponseEntity<Object> handleDataViewerErrors(DataViewerException exception, WebRequest request) {
        log.warn("Произошла ошибка в сервисе формирования данных для просмотра", exception);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JwtAuthenticationException.class)
    public ResponseEntity<Object> handleJwtAuthenticationException(JwtAuthenticationException exception, WebRequest request) {
        log.warn("Произошла ошибка при авторизации", exception);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Object> handleUsernameNotFoundException(UsernameNotFoundException exception, WebRequest request) {
        log.warn("Данный пользователь не зарегистрирован в системе", exception);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException exception, WebRequest request) {
        log.warn("Данный пользователь не зарегистрирован в системе", exception);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
    }

}

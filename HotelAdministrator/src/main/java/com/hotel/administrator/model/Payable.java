package com.hotel.administrator.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class Payable {

    @Column(name = "price", nullable = false)
    protected Integer price = 0;

}

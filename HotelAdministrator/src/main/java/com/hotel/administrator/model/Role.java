package com.hotel.administrator.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role {

    USER(Set.of(Permission.READ_DATA, Permission.REGISTER)),
    ADMIN(Set.of(Permission.READ_DATA, Permission.WRITE_DATA,
            Permission.EVICT_PERSON, Permission.SETTLE_PERSON, Permission.CRUD_USERS));

    private final Set<String> permissions;

    Role(Set<String> permissions) {
        this.permissions = permissions;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}

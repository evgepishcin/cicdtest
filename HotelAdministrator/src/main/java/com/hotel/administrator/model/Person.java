package com.hotel.administrator.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;
    @Column
    protected String name;
    @Column
    protected String surname;
    @Column
    protected String patronymic;
    @Column(name = "dateofbirth")
    protected LocalDate dateOfBirth;
    @Column
    protected String phone;
    @Column
    protected String email;
    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    protected Set<RoomHistory> roomHistories;
    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    protected Set<ServiceHistory> serviceHistories;

    public Person() {
    }

    public Person(Integer id) {
        this.id = id;
    }

    public Person(Integer id, String surname) {
        this.id = id;
        this.surname = surname;
    }

    public Person(Integer id, String name, String surname, String patronymic) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }


    @Override
    public String toString() {
        if (name == null && surname == null && patronymic == null) {
            return "БЕЗ ИМЕНИ";
        } else {
            String fullName = "";
            if (surname != null) {
                fullName += surname;
            }
            if (name != null) {
                fullName += " " + name;
            }
            if (patronymic != null) {
                fullName += " " + patronymic;
            }
            return fullName;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id.equals(person.id) &&
                Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(patronymic, person.patronymic) &&
                Objects.equals(dateOfBirth, person.dateOfBirth) &&
                Objects.equals(phone, person.phone) &&
                Objects.equals(email, person.email) &&
                Objects.equals(roomHistories, person.roomHistories) &&
                Objects.equals(serviceHistories, person.serviceHistories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

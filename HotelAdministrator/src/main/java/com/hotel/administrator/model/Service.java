package com.hotel.administrator.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "service")
public class Service extends Payable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id = 0;

    @Column(nullable = false)
    private String title;
    @Column
    private String about;
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    Set<ServiceHistory> serviceHistories = null;

    public Service() {
    }

    public Service(String title) {
        this.title = title;
    }

    public Service(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Service service = (Service) o;
        return id.equals(service.id) &&
                Objects.equals(title, service.title) &&
                Objects.equals(about, service.about) &&
                Objects.equals(serviceHistories, service.serviceHistories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }
}

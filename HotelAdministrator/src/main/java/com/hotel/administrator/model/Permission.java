package com.hotel.administrator.model;

public interface Permission {
    String SETTLE_PERSON = "settle_person";
    String EVICT_PERSON = "evict_person";
    String READ_DATA = "read_data";
    String WRITE_DATA = "write_data";
    String CRUD_USERS = "CRUD_USERS";
    String REGISTER = "register";
}

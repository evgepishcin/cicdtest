package com.hotel.administrator.model;

public enum BookingStatus {

    /**
     * Заявка на бронирование оккрыта и еще не рассмотрена
     */
    OPENED,

    /**
     * Бронь подтверждена
     */
    CONFIRMED,

    /**
     * Бронь отклонена
     */
    DECLINED
}

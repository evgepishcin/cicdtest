package com.hotel.administrator.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "room")
public class HotelRoom extends StarRating implements Comparable<HotelRoom> {
    @Column(name = "maxpersoncount", nullable = false)
    private Integer maxPersonCount = null;

    @Id
    @Column(name = "number", nullable = false)
    private Integer roomNumber = 0; //Так же играет роль id

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RoomStatus status = RoomStatus.SERVED;

    @OneToMany(mappedBy = "hotelRoom", fetch = FetchType.EAGER)
    private Set<RoomHistory> roomHistories = null;

    public enum RoomStatus {
        SERVED,
        REPAIRED
    }

    public HotelRoom() {
    }

    @Override
    public int compareTo(HotelRoom o) {
        return Integer.compare(maxPersonCount, o.maxPersonCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HotelRoom hotelRoom = (HotelRoom) o;
        return Objects.equals(maxPersonCount, hotelRoom.maxPersonCount) &&
                roomNumber.equals(hotelRoom.roomNumber) &&
                status == hotelRoom.status &&
                Objects.equals(roomHistories, hotelRoom.roomHistories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), roomNumber);
    }
}

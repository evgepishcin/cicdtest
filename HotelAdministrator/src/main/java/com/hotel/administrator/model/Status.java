package com.hotel.administrator.model;

public enum Status {
    ENABLED, DISABLED;
}

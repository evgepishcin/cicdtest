package com.hotel.administrator.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class StarRating extends Payable {

    @Column(name = "rating", nullable = false)
    protected Integer starCount = 0;

    public void setRating(Integer starCount) {
        this.starCount = starCount;
    }

    public int getRating() {
        return starCount;
    }
}

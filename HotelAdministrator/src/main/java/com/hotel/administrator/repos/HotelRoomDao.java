package com.hotel.administrator.repos;

import com.hotel.administrator.model.HotelRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface HotelRoomDao extends JpaRepository<HotelRoom, Integer> {

    @Query("select r from HotelRoom r left join RoomHistory h on r.roomNumber = h.hotelRoom.roomNumber where h.person.id = ?1 and h.endDate > ?2")
    HotelRoom getHotelRoomByPerson(int personId, LocalDateTime currentDate);

    @Query("select room from HotelRoom room where room.roomNumber not in (select r.roomNumber from HotelRoom r left join " +
            "RoomHistory history with r.roomNumber = history.hotelRoom.roomNumber where history.endDate >= ?1 " +
            "and history.startDate <= ?1)")
    List<HotelRoom> getEmptyHotelRooms(LocalDateTime currentDate);

    @Query("select room from HotelRoom room where room.status = 'SERVED' and room.roomNumber not in (select r.roomNumber from HotelRoom r left join " +
            "RoomHistory history with r.roomNumber = history.hotelRoom.roomNumber where history.endDate >= ?2 " +
            "and history.startDate <= ?1)")
    List<HotelRoom> getEmptyHotelRoomInPeriod(LocalDateTime startTime, LocalDateTime endTime);
}

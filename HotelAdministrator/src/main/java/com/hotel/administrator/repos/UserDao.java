package com.hotel.administrator.repos;

import com.hotel.administrator.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {

    User findByLogin(String login);
}

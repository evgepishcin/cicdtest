package com.hotel.administrator.repos;

import com.hotel.administrator.model.RoomHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface RoomHistoryDao extends JpaRepository<RoomHistory, Integer> {

    @Query(value = "delete from roomhistory " +
            "where id not in (" +
            "  select id" +
            "  from (" +
            "    select id" +
            "    from roomhistory" +
            "    order by end_date desc limit ?2" +
            "  ) foo" +
            ") and room_number = ?1 and end_date <= ?3"
            , nativeQuery = true)
    @Modifying
    void deleteOldRecords(int roomNumber, int keepCount, LocalDateTime currentDate);

    @Query("select history from RoomHistory history where history.person.id = ?1 and history.endDate > ?2 order by history.endDate desc")
    RoomHistory getRoomHistoryBySettledPerson(int personId, LocalDateTime currentDate);

    @Query(value = "select id, room_number, person_id, start_date, max(end_date) as end_date " +
            "FROM roomhistory where room_number not in(" +
            "select room_number from roomhistory where roomhistory.end_date > ?1" +
            ") group by room_number"
            , nativeQuery = true)
    List<RoomHistory> getWillBeFree(LocalDateTime date);

    @Query(value = "SELECT * FROM roomhistory where roomhistory.person_id = ?1 order by end_date desc limit 1", nativeQuery = true)
    RoomHistory getLastRoomHistoryByPersonId(int personId);

    @Query("select history from RoomHistory history where history.hotelRoom.roomNumber = ?1 order by history.endDate desc")
    List<RoomHistory> getRoomHistoryByRoomNumber(int roomNumber);

}

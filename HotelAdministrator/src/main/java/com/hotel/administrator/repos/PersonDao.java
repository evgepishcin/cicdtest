package com.hotel.administrator.repos;

import com.hotel.administrator.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface PersonDao extends JpaRepository<Person, Integer> {
    @Query("select p from Person p left join RoomHistory rh on p.id = rh.person.id where rh.endDate > ?1")
    List<Person> getSettledPersons(LocalDateTime currentDate);

    @Query("select distinct p, rh from Person p left join RoomHistory rh" +
            " on p.id = rh.person.id where rh.hotelRoom.roomNumber = ?1 and rh.endDate > ?2")
    List<Person> getHotelRoomPersons(int roomNumber, LocalDateTime currentDate);
}

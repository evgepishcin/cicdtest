package com.hotel.administrator.repos;

import com.hotel.administrator.model.ServiceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ServiceHistoryDao extends JpaRepository<ServiceHistory, Integer> {

    @Query("select history from ServiceHistory history where history.person.id = ?1 order by history.date desc")
    List<ServiceHistory> getServiceHistoryByPersonId(int personId);
}

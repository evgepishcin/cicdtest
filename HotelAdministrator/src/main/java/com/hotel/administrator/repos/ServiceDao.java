package com.hotel.administrator.repos;

import com.hotel.administrator.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceDao extends JpaRepository<Service, Integer> {
}

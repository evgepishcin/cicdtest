package com.hotel.administrator.comparators;

import com.hotel.administrator.model.Payable;

import java.util.Comparator;

public class PayableComparator implements Comparator<Payable> {

    @Override
    public int compare(Payable o1, Payable o2) {
        return Long.compare(o1.getPrice(), o2.getPrice());
    }
}

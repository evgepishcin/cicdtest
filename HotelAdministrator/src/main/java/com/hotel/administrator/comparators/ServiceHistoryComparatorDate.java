package com.hotel.administrator.comparators;

import com.hotel.administrator.model.ServiceHistory;

import java.util.Comparator;

public class ServiceHistoryComparatorDate implements Comparator<ServiceHistory> {
    @Override
    public int compare(ServiceHistory o1, ServiceHistory o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}

package com.hotel.administrator.comparators;

import com.hotel.administrator.model.RoomHistory;

import java.util.Comparator;

public class RoomHistoryComparatorEndDate implements Comparator<RoomHistory> {
    @Override
    public int compare(RoomHistory o1, RoomHistory o2) {
        return o1.getEndDate().compareTo(o2.getEndDate());
    }
}

package com.hotel.administrator.comparators;

import com.hotel.administrator.model.StarRating;

import java.util.Comparator;

public class StarRatingComparator implements Comparator<StarRating> {
    @Override
    public int compare(StarRating o1, StarRating o2) {
        return Integer.compare(o1.getRating(), o2.getRating());
    }
}

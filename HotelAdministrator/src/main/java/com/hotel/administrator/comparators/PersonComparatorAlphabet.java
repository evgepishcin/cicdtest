package com.hotel.administrator.comparators;

import com.hotel.administrator.model.Person;

import java.util.Comparator;

public class PersonComparatorAlphabet implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        String o1FullName = "";
        String o2FullName = "";
        if (o1.getSurname() != null) {
            o1FullName += o1.getSurname();
        }
        if (o2.getSurname() != null) {
            o2FullName += o2.getSurname();
        }
        if (o1.getName() != null) {
            o1FullName += o1.getName();
        }
        if (o2.getName() != null) {
            o2FullName += o2.getName();
        }
        if (o1.getPatronymic() != null) {
            o1FullName += o1.getPatronymic();
        }
        if (o2.getPatronymic() != null) {
            o2FullName += o2.getPatronymic();
        }
        o1FullName = o1FullName.toLowerCase();
        o2FullName = o2FullName.toLowerCase();
        return o1FullName.compareTo(o2FullName);
    }
}

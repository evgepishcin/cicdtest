package com.hotel.administrator.comparators;

import com.hotel.administrator.model.ServiceHistory;

import java.util.Comparator;

public class ServiceHistoryComparatorPrice implements Comparator<ServiceHistory> {

    @Override
    public int compare(ServiceHistory o1, ServiceHistory o2) {
        return o1.getService().getPrice().compareTo(o2.getService().getPrice());
    }
}

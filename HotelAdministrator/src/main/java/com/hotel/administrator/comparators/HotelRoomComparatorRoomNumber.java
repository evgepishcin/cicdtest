package com.hotel.administrator.comparators;

import com.hotel.administrator.model.HotelRoom;

import java.util.Comparator;

public class HotelRoomComparatorRoomNumber implements Comparator<HotelRoom> {

    @Override
    public int compare(HotelRoom o1, HotelRoom o2) {
        return Integer.compare(o1.getRoomNumber(), o2.getRoomNumber());
    }
}

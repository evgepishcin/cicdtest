package com.hotel.administrator.comparators;

import com.hotel.administrator.model.Service;

import java.util.Comparator;

public class ServiceComparatorTitle implements Comparator<Service> {
    @Override
    public int compare(Service o1, Service o2) {
        return o1.getTitle().compareTo(o2.getTitle());
    }
}
